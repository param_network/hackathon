var sop = {
  "contractName": "SOP",
  "abi": [
    {
      "constant": false,
      "inputs": [
        {
          "name": "subject",
          "type": "string"
        },
        {
          "name": "predicate",
          "type": "string"
        }
      ],
      "name": "pull",
      "outputs": [
        {
          "name": "",
          "type": "string"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "subject",
          "type": "string"
        },
        {
          "name": "predicate",
          "type": "string"
        },
        {
          "name": "object",
          "type": "string"
        }
      ],
      "name": "push",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "name": "_address",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    }
  ],
  "bytecode": "0x6060604052341561000f57600080fd5b6040516020806107b88339810160405280805160008054600160a060020a03909216600160a060020a03199092169190911790555050610764806100546000396000f30060606040526004361061004a5763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166248693b811461004f578063da76261614610159575b600080fd5b341561005a57600080fd5b6100e260046024813581810190830135806020601f8201819004810201604051908101604052818152929190602084018383808284378201915050505050509190803590602001908201803590602001908080601f01602080910402602001604051908101604052818152929190602084018383808284375094965061019195505050505050565b60405160208082528190810183818151815260200191508051906020019080838360005b8381101561011e578082015183820152602001610106565b50505050905090810190601f16801561014b5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561016457600080fd5b61018f60246004803582810192908201359181358083019290820135916044359182019101356103d3565b005b6101996105e1565b60006101a36105e1565b6101ab6105e1565b600086866040518083805190602001908083835b602083106101de5780518252601f1990920191602091820191016101bf565b6001836020036101000a038019825116818451161790925250505091909101905082805190602001908083835b6020831061022a5780518252601f19909201916020918201910161020b565b6001836020036101000a03801982511681845116179092525050509190910193506040925050505190819003902060008181526001602090815260409182902080549397509291828202909101905190810160405281815291906000602084015b8282101561034857838290600052602060002090018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103345780601f1061030957610100808354040283529160200191610334565b820191906000526020600020905b81548152906001019060200180831161031757829003601f168201915b50505050508152602001906001019061028b565b5050505092506020604051908101604052600080825290925090505b82518110156103c9576103bf83828151811061037c57fe5b9060200190602002015160408051908101604052600181527f600000000000000000000000000000000000000000000000000000000000000060208201526104d8565b9150600101610364565b5095945050505050565b6000805481903373ffffffffffffffffffffffffffffffffffffffff9081169116146103fe57600080fd5b87878787604051808585808284378201915050838380828437820191505094505050505060405180910390209150838387876040518085858082843782019150508383808284378201915050945050505050604051908190039020600083815260016020819052604090912080549293509190810161047d83826105f3565b600092835260209092206104939101868661061c565b505060008181526001602081905260409091208054909181016104b683826105f3565b600092835260209092206104cc91018a8a61061c565b50505050505050505050565b6104e06105e1565b6105016104ec83610508565b6104f585610508565b9063ffffffff61053016565b9392505050565b61051061069a565b602082016040805190810160405280845181526020019190915292915050565b6105386105e1565b6105406105e1565b600083518551016040518059106105545750595b818152601f19601f830116810160200160405290509150602082019050610581818660200151875161059c565b610594855182018560200151865161059c565b509392505050565b60005b602082106105c2578251845260208401935060208301925060208203915061059f565b6001826020036101000a03905080198351168185511617909352505050565b60206040519081016040526000815290565b815481835581811511610617576000838152602090206106179181019083016106b1565b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061065d5782800160ff1982351617855561068a565b8280016001018555821561068a579182015b8281111561068a57823582559160200191906001019061066f565b506106969291506106d7565b5090565b604080519081016040526000808252602082015290565b6106d491905b808211156106965760006106cb82826106f1565b506001016106b7565b90565b6106d491905b8082111561069657600081556001016106dd565b50805460018160011615610100020316600290046000825580601f106107175750610735565b601f01602090049060005260206000209081019061073591906106d7565b505600a165627a7a723058207aa31f1207b4e9149224ee9f6c238d4d5f03543a0624c458b0fb9244e6309cb80029",
  "deployedBytecode": "0x60606040526004361061004a5763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166248693b811461004f578063da76261614610159575b600080fd5b341561005a57600080fd5b6100e260046024813581810190830135806020601f8201819004810201604051908101604052818152929190602084018383808284378201915050505050509190803590602001908201803590602001908080601f01602080910402602001604051908101604052818152929190602084018383808284375094965061019195505050505050565b60405160208082528190810183818151815260200191508051906020019080838360005b8381101561011e578082015183820152602001610106565b50505050905090810190601f16801561014b5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561016457600080fd5b61018f60246004803582810192908201359181358083019290820135916044359182019101356103d3565b005b6101996105e1565b60006101a36105e1565b6101ab6105e1565b600086866040518083805190602001908083835b602083106101de5780518252601f1990920191602091820191016101bf565b6001836020036101000a038019825116818451161790925250505091909101905082805190602001908083835b6020831061022a5780518252601f19909201916020918201910161020b565b6001836020036101000a03801982511681845116179092525050509190910193506040925050505190819003902060008181526001602090815260409182902080549397509291828202909101905190810160405281815291906000602084015b8282101561034857838290600052602060002090018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103345780601f1061030957610100808354040283529160200191610334565b820191906000526020600020905b81548152906001019060200180831161031757829003601f168201915b50505050508152602001906001019061028b565b5050505092506020604051908101604052600080825290925090505b82518110156103c9576103bf83828151811061037c57fe5b9060200190602002015160408051908101604052600181527f600000000000000000000000000000000000000000000000000000000000000060208201526104d8565b9150600101610364565b5095945050505050565b6000805481903373ffffffffffffffffffffffffffffffffffffffff9081169116146103fe57600080fd5b87878787604051808585808284378201915050838380828437820191505094505050505060405180910390209150838387876040518085858082843782019150508383808284378201915050945050505050604051908190039020600083815260016020819052604090912080549293509190810161047d83826105f3565b600092835260209092206104939101868661061c565b505060008181526001602081905260409091208054909181016104b683826105f3565b600092835260209092206104cc91018a8a61061c565b50505050505050505050565b6104e06105e1565b6105016104ec83610508565b6104f585610508565b9063ffffffff61053016565b9392505050565b61051061069a565b602082016040805190810160405280845181526020019190915292915050565b6105386105e1565b6105406105e1565b600083518551016040518059106105545750595b818152601f19601f830116810160200160405290509150602082019050610581818660200151875161059c565b610594855182018560200151865161059c565b509392505050565b60005b602082106105c2578251845260208401935060208301925060208203915061059f565b6001826020036101000a03905080198351168185511617909352505050565b60206040519081016040526000815290565b815481835581811511610617576000838152602090206106179181019083016106b1565b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061065d5782800160ff1982351617855561068a565b8280016001018555821561068a579182015b8281111561068a57823582559160200191906001019061066f565b506106969291506106d7565b5090565b604080519081016040526000808252602082015290565b6106d491905b808211156106965760006106cb82826106f1565b506001016106b7565b90565b6106d491905b8082111561069657600081556001016106dd565b50805460018160011615610100020316600290046000825580601f106107175750610735565b601f01602090049060005260206000209081019061073591906106d7565b505600a165627a7a723058207aa31f1207b4e9149224ee9f6c238d4d5f03543a0624c458b0fb9244e6309cb80029",
  "sourceMap": "51:1306:7:-;;;182:84;;;;;;;;;;;;;;;;;;;;;;229:19;:30;;-1:-1:-1;;;;;229:30:7;;;-1:-1:-1;;;;;;229:30:7;;;;;;;;;-1:-1:-1;;51:1306:7;;;;;;",
  "deployedSourceMap": "51:1306:7:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;594:362;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;594:362:7;;-1:-1:-1;594:362:7;;-1:-1:-1;;;;;;594:362:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;23:1:-1;8:100;33:3;30:1;27:2;8:100;;;99:1;94:3;90;84:5;71:3;;;64:6;52:2;45:3;8:100;;;12:14;3:109;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;272:316:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;594:362;657:6;;:::i;:::-;674:15;730:20;;:::i;:::-;779:23;;:::i;:::-;821:10;702:7;710:9;692:28;;;;;;;;;;;;;36:153:-1;66:2;58;;36:153;;182:3;176:5;164:6;;-1:-1;;139:3;;;;98:2;89:3;;;;114;36:153;;;274:1;267:3;263:2;259:3;254;250;246;315:4;311:3;305;299:5;295:3;356:4;350:3;344:5;340:3;377:2;365:6;;;-1:-1;;;3:399;;;;;-1:-1;3:399;;;;;;;;;;36:153;66:2;58;;36:153;;182:3;176:5;164:6;;-1:-1;;139:3;;;;98:2;89:3;;;;114;36:153;;;274:1;267:3;263:2;259:3;254;250;246;315:4;311:3;305;299:5;295:3;356:4;350:3;344:5;340:3;377:2;365:6;;;-1:-1;;;3:399;;;;;-1:-1;3:399;;-1:-1;;;3:399;;;;;;;753:16:7;;;;:7;:16;;;;;;;;;730:39;;3:399:-1;;-1:-1;753:16:7;730:39;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;779:28;;;;;;;;;;;;;;-1:-1:-1;779:28:7;-1:-1:-1;817:107:7;843:4;:11;837:5;:17;817:107;;;890:23;897:4;902:5;897:11;;;;;;;;;;;;;;;;890:23;;;;;;;;;;;;;;;;:6;:23::i;:::-;878:35;-1:-1:-1;856:7:7;;817:107;;;-1:-1:-1;940:9:7;594:362;-1:-1:-1;;;;;594:362:7:o;272:316::-;405:14;375:19;;405:14;;361:10;375:19;361:33;;;375:19;;361:33;353:42;;;;;;432:7;;440:9;;422:28;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;405:45;;487:6;;494:9;;477:27;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;514:15;;;;:7;:15;;;;;;;;:28;;477:27;;-1:-1:-1;514:15:7;:28;;;;:15;:28;;:::i;:::-;;;;;;;;;;;;535:6;;514:28;:::i;:::-;-1:-1:-1;;552:15:7;;;;:7;:15;;;;;;;;:29;;:15;;:29;;;:15;:29;;:::i;:::-;;;;;;;;;;;;573:7;;552:29;:::i;:::-;;;272:316;;;;;;;;:::o;962:136::-;1028:6;;:::i;:::-;1052:39;1077:13;:3;:11;:13::i;:::-;1052:17;:7;:15;:17::i;:::-;:24;:39;:24;:39;:::i;:::-;1045:46;962:136;-1:-1:-1;;;962:136:7:o;2838:196:8:-;2891:5;;:::i;:::-;2966:4;2956:3;;2997:30;;;;;;;;;3009:4;3003:18;2997:30;;;;;;;;2990:37;2838:196;-1:-1:-1;;2838:196:8:o;23470:334::-;23534:6;;:::i;:::-;23552:17;;:::i;:::-;23616:11;23595:5;:10;23583:4;:9;:22;23572:34;;;;;;;;;;;;;-1:-1:-1;;23572:34:8;;;;;;;;;;;;23552:54;;23667:2;23662:3;23658;23648:22;;23681:36;23688:6;23696:4;:9;;;23707:4;:9;23681:6;:36::i;:::-;23727:50;23743:4;:9;23734:6;:18;23754:5;:10;;;23766:5;:10;23727:6;:50::i;:::-;-1:-1:-1;23794:3:8;23470:334;-1:-1:-1;;;23470:334:8:o;2090:548::-;2416:9;2210:164;2223:2;2216:9;;2210:164;;2298:3;2292:5;2286:4;2279:6;2338:2;2330:10;;;;2361:2;2354:9;;;;2234:2;2227:9;;;;2210:164;;;2448:1;2441:3;2436:2;:8;2428:3;:17;:21;2416:33;;2517:4;2513:3;2507;2501:5;2497:3;2569:4;2562;2556:5;2552:3;2600:2;2587:6;;;-1:-1:-1;;;2468:164:8:o;51:1306:7:-;;;;;;;;;;;;;:::o;:::-;;;;;;;;;;;;;;;;;;;;;;;;;;;:::i;:::-;;;;:::o;:::-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;;51:1306:7;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;-1:-1:-1;51:1306:7;;;-1:-1:-1;51:1306:7;:::i;:::-;;;:::o;:::-;;;;;;;;;;;;;;;;;;:::o;:::-;;;;;;;;;;;;;;;;:::i;:::-;;;;;;;;:::o;:::-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:::i;:::-;;:::o",
  "source": "pragma solidity ^0.4.18;\nimport \"../strings.sol\";\n\ncontract SOP {\n    using strings for *;\n    address private parentContractOwner;\n    mapping (bytes32=>string[]) dataMap;\n    \n    function SOP(address _address) public{\n        parentContractOwner = _address;\n    }\n\n    function push(string subject, string predicate, string object) external{\n        require(msg.sender == parentContractOwner);\n        bytes32 spHash = keccak256(subject,predicate);\n        bytes32 opHash = keccak256(object,predicate);\n        dataMap[spHash].push(object);\n        dataMap[opHash].push(subject);\n    }\n\n    function pull(string subject, string predicate) public returns(string){\n        bytes32 keyHash = keccak256(subject,predicate);\n        string[] memory data = dataMap[keyHash];\n        string memory finalData = \"\";\n        for(uint index = 0; index<data.length; index++){\n            finalData = append(data[index],\"`\");\n        }\n        return finalData;\n    }\n\n    function append(string mainStr, string str) pure internal returns(string){\n        return mainStr.toSlice().concat(str.toSlice());\n    }\n\n    // function getData(string productId, string action) public returns(string){\n    //     return pull(productId, action);\n    // }\n\n    // function getAllSellers(string productId) public returns(string){\n    //     return pull(productId,\"buy\");\n    // }\n}",
  "sourcePath": "/Users/divum/projects/blockcain/param/ecom/contracts/sop/sop.sol",
  "ast": {
    "attributes": {
      "absolutePath": "/Users/divum/projects/blockcain/param/ecom/contracts/sop/sop.sol",
      "exportedSymbols": {
        "SOP": [
          6031
        ]
      }
    },
    "children": [
      {
        "attributes": {
          "literals": [
            "solidity",
            "^",
            "0.4",
            ".18"
          ]
        },
        "id": 5893,
        "name": "PragmaDirective",
        "src": "0:24:7"
      },
      {
        "attributes": {
          "SourceUnit": 7719,
          "absolutePath": "/Users/divum/projects/blockcain/param/ecom/contracts/strings.sol",
          "file": "../strings.sol",
          "scope": 6032,
          "symbolAliases": [
            null
          ],
          "unitAlias": ""
        },
        "id": 5894,
        "name": "ImportDirective",
        "src": "25:24:7"
      },
      {
        "attributes": {
          "baseContracts": [
            null
          ],
          "contractDependencies": [
            null
          ],
          "contractKind": "contract",
          "documentation": null,
          "fullyImplemented": true,
          "linearizedBaseContracts": [
            6031
          ],
          "name": "SOP",
          "scope": 6032
        },
        "children": [
          {
            "attributes": {
              "typeName": null
            },
            "children": [
              {
                "attributes": {
                  "contractScope": null,
                  "name": "strings",
                  "referencedDeclaration": 7718,
                  "type": "library strings"
                },
                "id": 5895,
                "name": "UserDefinedTypeName",
                "src": "76:7:7"
              }
            ],
            "id": 5896,
            "name": "UsingForDirective",
            "src": "70:20:7"
          },
          {
            "attributes": {
              "constant": false,
              "name": "parentContractOwner",
              "scope": 6031,
              "stateVariable": true,
              "storageLocation": "default",
              "type": "address",
              "value": null,
              "visibility": "private"
            },
            "children": [
              {
                "attributes": {
                  "name": "address",
                  "type": "address"
                },
                "id": 5897,
                "name": "ElementaryTypeName",
                "src": "95:7:7"
              }
            ],
            "id": 5898,
            "name": "VariableDeclaration",
            "src": "95:35:7"
          },
          {
            "attributes": {
              "constant": false,
              "name": "dataMap",
              "scope": 6031,
              "stateVariable": true,
              "storageLocation": "default",
              "type": "mapping(bytes32 => string storage ref[] storage ref)",
              "value": null,
              "visibility": "internal"
            },
            "children": [
              {
                "attributes": {
                  "type": "mapping(bytes32 => string storage ref[] storage ref)"
                },
                "children": [
                  {
                    "attributes": {
                      "name": "bytes32",
                      "type": "bytes32"
                    },
                    "id": 5899,
                    "name": "ElementaryTypeName",
                    "src": "145:7:7"
                  },
                  {
                    "attributes": {
                      "length": null,
                      "type": "string storage ref[] storage pointer"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5900,
                        "name": "ElementaryTypeName",
                        "src": "154:6:7"
                      }
                    ],
                    "id": 5901,
                    "name": "ArrayTypeName",
                    "src": "154:8:7"
                  }
                ],
                "id": 5902,
                "name": "Mapping",
                "src": "136:27:7"
              }
            ],
            "id": 5903,
            "name": "VariableDeclaration",
            "src": "136:35:7"
          },
          {
            "attributes": {
              "constant": false,
              "implemented": true,
              "isConstructor": true,
              "modifiers": [
                null
              ],
              "name": "SOP",
              "payable": false,
              "scope": 6031,
              "stateMutability": "nonpayable",
              "superFunction": null,
              "visibility": "public"
            },
            "children": [
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "_address",
                      "scope": 5913,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "address",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "address",
                          "type": "address"
                        },
                        "id": 5904,
                        "name": "ElementaryTypeName",
                        "src": "195:7:7"
                      }
                    ],
                    "id": 5905,
                    "name": "VariableDeclaration",
                    "src": "195:16:7"
                  }
                ],
                "id": 5906,
                "name": "ParameterList",
                "src": "194:18:7"
              },
              {
                "attributes": {
                  "parameters": [
                    null
                  ]
                },
                "children": [],
                "id": 5907,
                "name": "ParameterList",
                "src": "219:0:7"
              },
              {
                "children": [
                  {
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "operator": "=",
                          "type": "address"
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5898,
                              "type": "address",
                              "value": "parentContractOwner"
                            },
                            "id": 5908,
                            "name": "Identifier",
                            "src": "229:19:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5905,
                              "type": "address",
                              "value": "_address"
                            },
                            "id": 5909,
                            "name": "Identifier",
                            "src": "251:8:7"
                          }
                        ],
                        "id": 5910,
                        "name": "Assignment",
                        "src": "229:30:7"
                      }
                    ],
                    "id": 5911,
                    "name": "ExpressionStatement",
                    "src": "229:30:7"
                  }
                ],
                "id": 5912,
                "name": "Block",
                "src": "219:47:7"
              }
            ],
            "id": 5913,
            "name": "FunctionDefinition",
            "src": "182:84:7"
          },
          {
            "attributes": {
              "constant": false,
              "implemented": true,
              "isConstructor": false,
              "modifiers": [
                null
              ],
              "name": "push",
              "payable": false,
              "scope": 6031,
              "stateMutability": "nonpayable",
              "superFunction": null,
              "visibility": "external"
            },
            "children": [
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "subject",
                      "scope": 5958,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string calldata",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5914,
                        "name": "ElementaryTypeName",
                        "src": "286:6:7"
                      }
                    ],
                    "id": 5915,
                    "name": "VariableDeclaration",
                    "src": "286:14:7"
                  },
                  {
                    "attributes": {
                      "constant": false,
                      "name": "predicate",
                      "scope": 5958,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string calldata",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5916,
                        "name": "ElementaryTypeName",
                        "src": "302:6:7"
                      }
                    ],
                    "id": 5917,
                    "name": "VariableDeclaration",
                    "src": "302:16:7"
                  },
                  {
                    "attributes": {
                      "constant": false,
                      "name": "object",
                      "scope": 5958,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string calldata",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5918,
                        "name": "ElementaryTypeName",
                        "src": "320:6:7"
                      }
                    ],
                    "id": 5919,
                    "name": "VariableDeclaration",
                    "src": "320:13:7"
                  }
                ],
                "id": 5920,
                "name": "ParameterList",
                "src": "285:49:7"
              },
              {
                "attributes": {
                  "parameters": [
                    null
                  ]
                },
                "children": [],
                "id": 5921,
                "name": "ParameterList",
                "src": "343:0:7"
              },
              {
                "children": [
                  {
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "tuple()",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_bool",
                                  "typeString": "bool"
                                }
                              ],
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 8456,
                              "type": "function (bool) pure",
                              "value": "require"
                            },
                            "id": 5922,
                            "name": "Identifier",
                            "src": "353:7:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "commonType": {
                                "typeIdentifier": "t_address",
                                "typeString": "address"
                              },
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "operator": "==",
                              "type": "bool"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "member_name": "sender",
                                  "referencedDeclaration": null,
                                  "type": "address"
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 8453,
                                      "type": "msg",
                                      "value": "msg"
                                    },
                                    "id": 5923,
                                    "name": "Identifier",
                                    "src": "361:3:7"
                                  }
                                ],
                                "id": 5924,
                                "name": "MemberAccess",
                                "src": "361:10:7"
                              },
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "overloadedDeclarations": [
                                    null
                                  ],
                                  "referencedDeclaration": 5898,
                                  "type": "address",
                                  "value": "parentContractOwner"
                                },
                                "id": 5925,
                                "name": "Identifier",
                                "src": "375:19:7"
                              }
                            ],
                            "id": 5926,
                            "name": "BinaryOperation",
                            "src": "361:33:7"
                          }
                        ],
                        "id": 5927,
                        "name": "FunctionCall",
                        "src": "353:42:7"
                      }
                    ],
                    "id": 5928,
                    "name": "ExpressionStatement",
                    "src": "353:42:7"
                  },
                  {
                    "attributes": {
                      "assignments": [
                        5930
                      ]
                    },
                    "children": [
                      {
                        "attributes": {
                          "constant": false,
                          "name": "spHash",
                          "scope": 5958,
                          "stateVariable": false,
                          "storageLocation": "default",
                          "type": "bytes32",
                          "value": null,
                          "visibility": "internal"
                        },
                        "children": [
                          {
                            "attributes": {
                              "name": "bytes32",
                              "type": "bytes32"
                            },
                            "id": 5929,
                            "name": "ElementaryTypeName",
                            "src": "405:7:7"
                          }
                        ],
                        "id": 5930,
                        "name": "VariableDeclaration",
                        "src": "405:14:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "bytes32",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                },
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                }
                              ],
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 8447,
                              "type": "function () pure returns (bytes32)",
                              "value": "keccak256"
                            },
                            "id": 5931,
                            "name": "Identifier",
                            "src": "422:9:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5915,
                              "type": "string calldata",
                              "value": "subject"
                            },
                            "id": 5932,
                            "name": "Identifier",
                            "src": "432:7:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5917,
                              "type": "string calldata",
                              "value": "predicate"
                            },
                            "id": 5933,
                            "name": "Identifier",
                            "src": "440:9:7"
                          }
                        ],
                        "id": 5934,
                        "name": "FunctionCall",
                        "src": "422:28:7"
                      }
                    ],
                    "id": 5935,
                    "name": "VariableDeclarationStatement",
                    "src": "405:45:7"
                  },
                  {
                    "attributes": {
                      "assignments": [
                        5937
                      ]
                    },
                    "children": [
                      {
                        "attributes": {
                          "constant": false,
                          "name": "opHash",
                          "scope": 5958,
                          "stateVariable": false,
                          "storageLocation": "default",
                          "type": "bytes32",
                          "value": null,
                          "visibility": "internal"
                        },
                        "children": [
                          {
                            "attributes": {
                              "name": "bytes32",
                              "type": "bytes32"
                            },
                            "id": 5936,
                            "name": "ElementaryTypeName",
                            "src": "460:7:7"
                          }
                        ],
                        "id": 5937,
                        "name": "VariableDeclaration",
                        "src": "460:14:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "bytes32",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                },
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                }
                              ],
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 8447,
                              "type": "function () pure returns (bytes32)",
                              "value": "keccak256"
                            },
                            "id": 5938,
                            "name": "Identifier",
                            "src": "477:9:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5919,
                              "type": "string calldata",
                              "value": "object"
                            },
                            "id": 5939,
                            "name": "Identifier",
                            "src": "487:6:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5917,
                              "type": "string calldata",
                              "value": "predicate"
                            },
                            "id": 5940,
                            "name": "Identifier",
                            "src": "494:9:7"
                          }
                        ],
                        "id": 5941,
                        "name": "FunctionCall",
                        "src": "477:27:7"
                      }
                    ],
                    "id": 5942,
                    "name": "VariableDeclarationStatement",
                    "src": "460:44:7"
                  },
                  {
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "uint256",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                }
                              ],
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "member_name": "push",
                              "referencedDeclaration": null,
                              "type": "function (string storage ref) returns (uint256)"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "type": "string storage ref[] storage ref"
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 5903,
                                      "type": "mapping(bytes32 => string storage ref[] storage ref)",
                                      "value": "dataMap"
                                    },
                                    "id": 5943,
                                    "name": "Identifier",
                                    "src": "514:7:7"
                                  },
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 5930,
                                      "type": "bytes32",
                                      "value": "spHash"
                                    },
                                    "id": 5944,
                                    "name": "Identifier",
                                    "src": "522:6:7"
                                  }
                                ],
                                "id": 5945,
                                "name": "IndexAccess",
                                "src": "514:15:7"
                              }
                            ],
                            "id": 5946,
                            "name": "MemberAccess",
                            "src": "514:20:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5919,
                              "type": "string calldata",
                              "value": "object"
                            },
                            "id": 5947,
                            "name": "Identifier",
                            "src": "535:6:7"
                          }
                        ],
                        "id": 5948,
                        "name": "FunctionCall",
                        "src": "514:28:7"
                      }
                    ],
                    "id": 5949,
                    "name": "ExpressionStatement",
                    "src": "514:28:7"
                  },
                  {
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "uint256",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_string_calldata_ptr",
                                  "typeString": "string calldata"
                                }
                              ],
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "member_name": "push",
                              "referencedDeclaration": null,
                              "type": "function (string storage ref) returns (uint256)"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "isConstant": false,
                                  "isLValue": true,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "type": "string storage ref[] storage ref"
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 5903,
                                      "type": "mapping(bytes32 => string storage ref[] storage ref)",
                                      "value": "dataMap"
                                    },
                                    "id": 5950,
                                    "name": "Identifier",
                                    "src": "552:7:7"
                                  },
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 5937,
                                      "type": "bytes32",
                                      "value": "opHash"
                                    },
                                    "id": 5951,
                                    "name": "Identifier",
                                    "src": "560:6:7"
                                  }
                                ],
                                "id": 5952,
                                "name": "IndexAccess",
                                "src": "552:15:7"
                              }
                            ],
                            "id": 5953,
                            "name": "MemberAccess",
                            "src": "552:20:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5915,
                              "type": "string calldata",
                              "value": "subject"
                            },
                            "id": 5954,
                            "name": "Identifier",
                            "src": "573:7:7"
                          }
                        ],
                        "id": 5955,
                        "name": "FunctionCall",
                        "src": "552:29:7"
                      }
                    ],
                    "id": 5956,
                    "name": "ExpressionStatement",
                    "src": "552:29:7"
                  }
                ],
                "id": 5957,
                "name": "Block",
                "src": "343:245:7"
              }
            ],
            "id": 5958,
            "name": "FunctionDefinition",
            "src": "272:316:7"
          },
          {
            "attributes": {
              "constant": false,
              "implemented": true,
              "isConstructor": false,
              "modifiers": [
                null
              ],
              "name": "pull",
              "payable": false,
              "scope": 6031,
              "stateMutability": "nonpayable",
              "superFunction": null,
              "visibility": "public"
            },
            "children": [
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "subject",
                      "scope": 6011,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5959,
                        "name": "ElementaryTypeName",
                        "src": "608:6:7"
                      }
                    ],
                    "id": 5960,
                    "name": "VariableDeclaration",
                    "src": "608:14:7"
                  },
                  {
                    "attributes": {
                      "constant": false,
                      "name": "predicate",
                      "scope": 6011,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5961,
                        "name": "ElementaryTypeName",
                        "src": "624:6:7"
                      }
                    ],
                    "id": 5962,
                    "name": "VariableDeclaration",
                    "src": "624:16:7"
                  }
                ],
                "id": 5963,
                "name": "ParameterList",
                "src": "607:34:7"
              },
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "",
                      "scope": 6011,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 5964,
                        "name": "ElementaryTypeName",
                        "src": "657:6:7"
                      }
                    ],
                    "id": 5965,
                    "name": "VariableDeclaration",
                    "src": "657:6:7"
                  }
                ],
                "id": 5966,
                "name": "ParameterList",
                "src": "656:8:7"
              },
              {
                "children": [
                  {
                    "attributes": {
                      "assignments": [
                        5968
                      ]
                    },
                    "children": [
                      {
                        "attributes": {
                          "constant": false,
                          "name": "keyHash",
                          "scope": 6011,
                          "stateVariable": false,
                          "storageLocation": "default",
                          "type": "bytes32",
                          "value": null,
                          "visibility": "internal"
                        },
                        "children": [
                          {
                            "attributes": {
                              "name": "bytes32",
                              "type": "bytes32"
                            },
                            "id": 5967,
                            "name": "ElementaryTypeName",
                            "src": "674:7:7"
                          }
                        ],
                        "id": 5968,
                        "name": "VariableDeclaration",
                        "src": "674:15:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "bytes32",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_string_memory_ptr",
                                  "typeString": "string memory"
                                },
                                {
                                  "typeIdentifier": "t_string_memory_ptr",
                                  "typeString": "string memory"
                                }
                              ],
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 8447,
                              "type": "function () pure returns (bytes32)",
                              "value": "keccak256"
                            },
                            "id": 5969,
                            "name": "Identifier",
                            "src": "692:9:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5960,
                              "type": "string memory",
                              "value": "subject"
                            },
                            "id": 5970,
                            "name": "Identifier",
                            "src": "702:7:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5962,
                              "type": "string memory",
                              "value": "predicate"
                            },
                            "id": 5971,
                            "name": "Identifier",
                            "src": "710:9:7"
                          }
                        ],
                        "id": 5972,
                        "name": "FunctionCall",
                        "src": "692:28:7"
                      }
                    ],
                    "id": 5973,
                    "name": "VariableDeclarationStatement",
                    "src": "674:46:7"
                  },
                  {
                    "attributes": {
                      "assignments": [
                        5977
                      ]
                    },
                    "children": [
                      {
                        "attributes": {
                          "constant": false,
                          "name": "data",
                          "scope": 6011,
                          "stateVariable": false,
                          "storageLocation": "memory",
                          "type": "string memory[] memory",
                          "value": null,
                          "visibility": "internal"
                        },
                        "children": [
                          {
                            "attributes": {
                              "length": null,
                              "type": "string storage ref[] storage pointer"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "name": "string",
                                  "type": "string storage pointer"
                                },
                                "id": 5975,
                                "name": "ElementaryTypeName",
                                "src": "730:6:7"
                              }
                            ],
                            "id": 5976,
                            "name": "ArrayTypeName",
                            "src": "730:8:7"
                          }
                        ],
                        "id": 5977,
                        "name": "VariableDeclaration",
                        "src": "730:20:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "type": "string storage ref[] storage ref"
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5903,
                              "type": "mapping(bytes32 => string storage ref[] storage ref)",
                              "value": "dataMap"
                            },
                            "id": 5978,
                            "name": "Identifier",
                            "src": "753:7:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5968,
                              "type": "bytes32",
                              "value": "keyHash"
                            },
                            "id": 5979,
                            "name": "Identifier",
                            "src": "761:7:7"
                          }
                        ],
                        "id": 5980,
                        "name": "IndexAccess",
                        "src": "753:16:7"
                      }
                    ],
                    "id": 5981,
                    "name": "VariableDeclarationStatement",
                    "src": "730:39:7"
                  },
                  {
                    "attributes": {
                      "assignments": [
                        5983
                      ]
                    },
                    "children": [
                      {
                        "attributes": {
                          "constant": false,
                          "name": "finalData",
                          "scope": 6011,
                          "stateVariable": false,
                          "storageLocation": "memory",
                          "type": "string memory",
                          "value": null,
                          "visibility": "internal"
                        },
                        "children": [
                          {
                            "attributes": {
                              "name": "string",
                              "type": "string storage pointer"
                            },
                            "id": 5982,
                            "name": "ElementaryTypeName",
                            "src": "779:6:7"
                          }
                        ],
                        "id": 5983,
                        "name": "VariableDeclaration",
                        "src": "779:23:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "hexvalue": "",
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "lValueRequested": false,
                          "subdenomination": null,
                          "token": "string",
                          "type": "literal_string \"\"",
                          "value": ""
                        },
                        "id": 5984,
                        "name": "Literal",
                        "src": "805:2:7"
                      }
                    ],
                    "id": 5985,
                    "name": "VariableDeclarationStatement",
                    "src": "779:28:7"
                  },
                  {
                    "children": [
                      {
                        "attributes": {
                          "assignments": [
                            5987
                          ]
                        },
                        "children": [
                          {
                            "attributes": {
                              "constant": false,
                              "name": "index",
                              "scope": 6011,
                              "stateVariable": false,
                              "storageLocation": "default",
                              "type": "uint256",
                              "value": null,
                              "visibility": "internal"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "name": "uint",
                                  "type": "uint256"
                                },
                                "id": 5986,
                                "name": "ElementaryTypeName",
                                "src": "821:4:7"
                              }
                            ],
                            "id": 5987,
                            "name": "VariableDeclaration",
                            "src": "821:10:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "hexvalue": "30",
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": true,
                              "lValueRequested": false,
                              "subdenomination": null,
                              "token": "number",
                              "type": "int_const 0",
                              "value": "0"
                            },
                            "id": 5988,
                            "name": "Literal",
                            "src": "834:1:7"
                          }
                        ],
                        "id": 5989,
                        "name": "VariableDeclarationStatement",
                        "src": "821:14:7"
                      },
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "commonType": {
                            "typeIdentifier": "t_uint256",
                            "typeString": "uint256"
                          },
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "lValueRequested": false,
                          "operator": "<",
                          "type": "bool"
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "overloadedDeclarations": [
                                null
                              ],
                              "referencedDeclaration": 5987,
                              "type": "uint256",
                              "value": "index"
                            },
                            "id": 5990,
                            "name": "Identifier",
                            "src": "837:5:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "member_name": "length",
                              "referencedDeclaration": null,
                              "type": "uint256"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "overloadedDeclarations": [
                                    null
                                  ],
                                  "referencedDeclaration": 5977,
                                  "type": "string memory[] memory",
                                  "value": "data"
                                },
                                "id": 5991,
                                "name": "Identifier",
                                "src": "843:4:7"
                              }
                            ],
                            "id": 5992,
                            "name": "MemberAccess",
                            "src": "843:11:7"
                          }
                        ],
                        "id": 5993,
                        "name": "BinaryOperation",
                        "src": "837:17:7"
                      },
                      {
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "lValueRequested": false,
                              "operator": "++",
                              "prefix": false,
                              "type": "uint256"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "overloadedDeclarations": [
                                    null
                                  ],
                                  "referencedDeclaration": 5987,
                                  "type": "uint256",
                                  "value": "index"
                                },
                                "id": 5994,
                                "name": "Identifier",
                                "src": "856:5:7"
                              }
                            ],
                            "id": 5995,
                            "name": "UnaryOperation",
                            "src": "856:7:7"
                          }
                        ],
                        "id": 5996,
                        "name": "ExpressionStatement",
                        "src": "856:7:7"
                      },
                      {
                        "children": [
                          {
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "operator": "=",
                                  "type": "string memory"
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 5983,
                                      "type": "string memory",
                                      "value": "finalData"
                                    },
                                    "id": 5997,
                                    "name": "Identifier",
                                    "src": "878:9:7"
                                  },
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "isConstant": false,
                                      "isLValue": false,
                                      "isPure": false,
                                      "isStructConstructorCall": false,
                                      "lValueRequested": false,
                                      "names": [
                                        null
                                      ],
                                      "type": "string memory",
                                      "type_conversion": false
                                    },
                                    "children": [
                                      {
                                        "attributes": {
                                          "argumentTypes": [
                                            {
                                              "typeIdentifier": "t_string_memory",
                                              "typeString": "string memory"
                                            },
                                            {
                                              "typeIdentifier": "t_stringliteral_15a5de5d00dfc39d199ee772e89858c204d1d545de092db54a345c7303942607",
                                              "typeString": "literal_string \"`\""
                                            }
                                          ],
                                          "overloadedDeclarations": [
                                            null
                                          ],
                                          "referencedDeclaration": 6030,
                                          "type": "function (string memory,string memory) pure returns (string memory)",
                                          "value": "append"
                                        },
                                        "id": 5998,
                                        "name": "Identifier",
                                        "src": "890:6:7"
                                      },
                                      {
                                        "attributes": {
                                          "argumentTypes": null,
                                          "isConstant": false,
                                          "isLValue": true,
                                          "isPure": false,
                                          "lValueRequested": false,
                                          "type": "string memory"
                                        },
                                        "children": [
                                          {
                                            "attributes": {
                                              "argumentTypes": null,
                                              "overloadedDeclarations": [
                                                null
                                              ],
                                              "referencedDeclaration": 5977,
                                              "type": "string memory[] memory",
                                              "value": "data"
                                            },
                                            "id": 5999,
                                            "name": "Identifier",
                                            "src": "897:4:7"
                                          },
                                          {
                                            "attributes": {
                                              "argumentTypes": null,
                                              "overloadedDeclarations": [
                                                null
                                              ],
                                              "referencedDeclaration": 5987,
                                              "type": "uint256",
                                              "value": "index"
                                            },
                                            "id": 6000,
                                            "name": "Identifier",
                                            "src": "902:5:7"
                                          }
                                        ],
                                        "id": 6001,
                                        "name": "IndexAccess",
                                        "src": "897:11:7"
                                      },
                                      {
                                        "attributes": {
                                          "argumentTypes": null,
                                          "hexvalue": "60",
                                          "isConstant": false,
                                          "isLValue": false,
                                          "isPure": true,
                                          "lValueRequested": false,
                                          "subdenomination": null,
                                          "token": "string",
                                          "type": "literal_string \"`\"",
                                          "value": "`"
                                        },
                                        "id": 6002,
                                        "name": "Literal",
                                        "src": "909:3:7"
                                      }
                                    ],
                                    "id": 6003,
                                    "name": "FunctionCall",
                                    "src": "890:23:7"
                                  }
                                ],
                                "id": 6004,
                                "name": "Assignment",
                                "src": "878:35:7"
                              }
                            ],
                            "id": 6005,
                            "name": "ExpressionStatement",
                            "src": "878:35:7"
                          }
                        ],
                        "id": 6006,
                        "name": "Block",
                        "src": "864:60:7"
                      }
                    ],
                    "id": 6007,
                    "name": "ForStatement",
                    "src": "817:107:7"
                  },
                  {
                    "attributes": {
                      "functionReturnParameters": 5966
                    },
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "overloadedDeclarations": [
                            null
                          ],
                          "referencedDeclaration": 5983,
                          "type": "string memory",
                          "value": "finalData"
                        },
                        "id": 6008,
                        "name": "Identifier",
                        "src": "940:9:7"
                      }
                    ],
                    "id": 6009,
                    "name": "Return",
                    "src": "933:16:7"
                  }
                ],
                "id": 6010,
                "name": "Block",
                "src": "664:292:7"
              }
            ],
            "id": 6011,
            "name": "FunctionDefinition",
            "src": "594:362:7"
          },
          {
            "attributes": {
              "constant": true,
              "implemented": true,
              "isConstructor": false,
              "modifiers": [
                null
              ],
              "name": "append",
              "payable": false,
              "scope": 6031,
              "stateMutability": "pure",
              "superFunction": null,
              "visibility": "internal"
            },
            "children": [
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "mainStr",
                      "scope": 6030,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 6012,
                        "name": "ElementaryTypeName",
                        "src": "978:6:7"
                      }
                    ],
                    "id": 6013,
                    "name": "VariableDeclaration",
                    "src": "978:14:7"
                  },
                  {
                    "attributes": {
                      "constant": false,
                      "name": "str",
                      "scope": 6030,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 6014,
                        "name": "ElementaryTypeName",
                        "src": "994:6:7"
                      }
                    ],
                    "id": 6015,
                    "name": "VariableDeclaration",
                    "src": "994:10:7"
                  }
                ],
                "id": 6016,
                "name": "ParameterList",
                "src": "977:28:7"
              },
              {
                "children": [
                  {
                    "attributes": {
                      "constant": false,
                      "name": "",
                      "scope": 6030,
                      "stateVariable": false,
                      "storageLocation": "default",
                      "type": "string memory",
                      "value": null,
                      "visibility": "internal"
                    },
                    "children": [
                      {
                        "attributes": {
                          "name": "string",
                          "type": "string storage pointer"
                        },
                        "id": 6017,
                        "name": "ElementaryTypeName",
                        "src": "1028:6:7"
                      }
                    ],
                    "id": 6018,
                    "name": "VariableDeclaration",
                    "src": "1028:6:7"
                  }
                ],
                "id": 6019,
                "name": "ParameterList",
                "src": "1027:8:7"
              },
              {
                "children": [
                  {
                    "attributes": {
                      "functionReturnParameters": 6019
                    },
                    "children": [
                      {
                        "attributes": {
                          "argumentTypes": null,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": false,
                          "isStructConstructorCall": false,
                          "lValueRequested": false,
                          "names": [
                            null
                          ],
                          "type": "string memory",
                          "type_conversion": false
                        },
                        "children": [
                          {
                            "attributes": {
                              "argumentTypes": [
                                {
                                  "typeIdentifier": "t_struct$_slice_$6038_memory_ptr",
                                  "typeString": "struct strings.slice memory"
                                }
                              ],
                              "isConstant": false,
                              "isLValue": true,
                              "isPure": false,
                              "lValueRequested": false,
                              "member_name": "concat",
                              "referencedDeclaration": 7603,
                              "type": "function (struct strings.slice memory,struct strings.slice memory) pure returns (string memory)"
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": null,
                                  "arguments": [
                                    null
                                  ],
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "isStructConstructorCall": false,
                                  "lValueRequested": false,
                                  "names": [
                                    null
                                  ],
                                  "type": "struct strings.slice memory",
                                  "type_conversion": false
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": [
                                        null
                                      ],
                                      "isConstant": false,
                                      "isLValue": false,
                                      "isPure": false,
                                      "lValueRequested": false,
                                      "member_name": "toSlice",
                                      "referencedDeclaration": 6098,
                                      "type": "function (string memory) pure returns (struct strings.slice memory)"
                                    },
                                    "children": [
                                      {
                                        "attributes": {
                                          "argumentTypes": null,
                                          "overloadedDeclarations": [
                                            null
                                          ],
                                          "referencedDeclaration": 6013,
                                          "type": "string memory",
                                          "value": "mainStr"
                                        },
                                        "id": 6020,
                                        "name": "Identifier",
                                        "src": "1052:7:7"
                                      }
                                    ],
                                    "id": 6021,
                                    "name": "MemberAccess",
                                    "src": "1052:15:7"
                                  }
                                ],
                                "id": 6022,
                                "name": "FunctionCall",
                                "src": "1052:17:7"
                              }
                            ],
                            "id": 6023,
                            "name": "MemberAccess",
                            "src": "1052:24:7"
                          },
                          {
                            "attributes": {
                              "argumentTypes": null,
                              "arguments": [
                                null
                              ],
                              "isConstant": false,
                              "isLValue": false,
                              "isPure": false,
                              "isStructConstructorCall": false,
                              "lValueRequested": false,
                              "names": [
                                null
                              ],
                              "type": "struct strings.slice memory",
                              "type_conversion": false
                            },
                            "children": [
                              {
                                "attributes": {
                                  "argumentTypes": [
                                    null
                                  ],
                                  "isConstant": false,
                                  "isLValue": false,
                                  "isPure": false,
                                  "lValueRequested": false,
                                  "member_name": "toSlice",
                                  "referencedDeclaration": 6098,
                                  "type": "function (string memory) pure returns (struct strings.slice memory)"
                                },
                                "children": [
                                  {
                                    "attributes": {
                                      "argumentTypes": null,
                                      "overloadedDeclarations": [
                                        null
                                      ],
                                      "referencedDeclaration": 6015,
                                      "type": "string memory",
                                      "value": "str"
                                    },
                                    "id": 6024,
                                    "name": "Identifier",
                                    "src": "1077:3:7"
                                  }
                                ],
                                "id": 6025,
                                "name": "MemberAccess",
                                "src": "1077:11:7"
                              }
                            ],
                            "id": 6026,
                            "name": "FunctionCall",
                            "src": "1077:13:7"
                          }
                        ],
                        "id": 6027,
                        "name": "FunctionCall",
                        "src": "1052:39:7"
                      }
                    ],
                    "id": 6028,
                    "name": "Return",
                    "src": "1045:46:7"
                  }
                ],
                "id": 6029,
                "name": "Block",
                "src": "1035:63:7"
              }
            ],
            "id": 6030,
            "name": "FunctionDefinition",
            "src": "962:136:7"
          }
        ],
        "id": 6031,
        "name": "ContractDefinition",
        "src": "51:1306:7"
      }
    ],
    "id": 6032,
    "name": "SourceUnit",
    "src": "0:1357:7"
  },
  "compiler": {
    "name": "solc",
    "version": "0.4.18+commit.9cf6e910.Emscripten.clang"
  },
  "networks": {
    "72616": {
      "events": {},
      "links": {},
      "address": "0x2e62b85fb87298d4b248024d7e220a35d4e8d505"
    }
  },
  "schemaVersion": "1.0.1",
  "updatedAt": "2018-05-31T11:20:46.972Z"
}

sopABI = web3.eth.contract(sop.abi);
sopContract = sopABI.at('0x2e62b85fb87298d4b248024d7e220a35d4e8d505');
manufacturers= [{name:"Apple",prods:["Apple MacBook Air MQD32HN/A 13.3-inch Laptop 2017", 'Apple iPad(6th Gen) Tablet (9.7 inch&comma;32GB&comma; Wi-Fi)']},
                {name:"Amazon",prods:['Echo Dot']},
                {name:"Sony",prods:["Sony 101.4 cm TV",'The Last Of Us: Remastered (PS4)','Call of Duty: Infinite Warfare (PS4)']},
                {name:"HP",prods:['HP 14q-BU005TU 2017']},
                {name:"iBall",prods:['iBall CompBook Exemplaire']},
                {name:"HyperX",prods:['HyperX Cloud Stinger Gaming Headset']},
                {name:"Canon",prods:['Canon EOS 1300D 18MP','Canon EOS 1500D 24.1MP']},
                {name:"WD",prods:['WD My Passport 1TB',]},
                {name:"Zebronics",prods:['Zebronics BT4440RUCF 4.1']}];
function initManufacturers(){
    personal.unlockAccount(eth.accounts[0],'abc123');
    for(var index=0;index<manufacturers.length;index++){
        var _gas = sopContract.push.estimateGas("manufacturer","name",manufacturers[index].name, {from:eth.accounts[0]});
        sopContract.push("manufacturer","name",manufacturers[index].name, {from:eth.accounts[0], gas:parseInt(_gas*1.3)})
    }
}

function initProducts(){
    personal.unlockAccount(eth.accounts[0],'abc123');
    for(var index=0;index<manufacturers.length;index++){
        console.log("Index:",index);
        var prods = manufacturers[index].prods;
        var name = manufacturers[index].name;
        for(var pId=0;pId<prods.length;pId++){
            var _gas = sopContract.push.estimateGas(name,"product",prods[pId], {from:eth.accounts[0]});
            sopContract.push(name,"product",prods[pId], {from:eth.accounts[0], gas:parseInt(_gas*1.3)})
        }
    }
}