pragma solidity ^0.4.18;

interface ISOP {
    function push(string subject, string predicate, string object) external;
}