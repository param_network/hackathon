pragma solidity ^0.4.18;
import "../utils/strings.sol";

contract SOP {
    using strings for *;

    address private parentContractOwner;
    address private owner;

    mapping (bytes32=>uint[]) private dataMap;
    // mapping (string=>uint) private mapOfKeyIndexs;

    string[] private data;

    function push(string subject, string predicate, string object) external{
        bytes32 spHash = keccak256(subject, predicate);
        bytes32 opHash = keccak256(object, predicate);
        dataMap[spHash].push(pushValue(object));
        dataMap[opHash].push(pushValue(subject));
    }

    function pushValue(string str) private returns(uint){
        uint index;// = mapOfKeyIndexs[str];
        if(data.length!=0 && keccak256(str) == keccak256(data[index])) {
            return index;
        }
        index = data.length;
        // mapOfKeyIndexs[str] = index;
        data.push(str);
        return index;
    }

    function pull(string subject, string predicate) public returns(string){
        bytes32 keyHash = keccak256(subject,predicate);
        uint[] memory dataIndexs = dataMap[keyHash];
        string memory finalData = "";
        for(uint index = 0; index<dataIndexs.length; index++){
            finalData = append(finalData,data[dataIndexs[index]]);
            finalData = append(finalData,"`");
        }
        return finalData;
    }

    function append(string mainStr, string str) pure internal returns(string){
        return mainStr.toSlice().concat(str.toSlice());
    }

    function updateParentContractOwner(address _addr) public{
        require(owner == msg.sender);
        parentContractOwner = _addr;
    }
}
