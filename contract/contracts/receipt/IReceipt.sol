pragma solidity ^0.4.18;

interface IReceipt {
    
    function initiateProposal(address _buyerId, string  _productsCSV, uint _discountPercent, uint _taxPercent, uint discountAmount, uint taxAmount, uint subTotal, uint netTotal) external returns(uint256);

    function acceptProposal(uint256 orderId, address sellerId) external;

    function rejectProposal(uint256 orderId, address sellerId) external;

    function sendInvoice(uint256 orderId, address buyerId) external returns(bool);

    function paymentSuccess(uint256 orderId, address sellerId) external returns(bool);

    function paymentFail(uint256 orderId, address sellerId) external returns(bool);

    function createReceipt(uint256 orderId, address buyerId) external;

    function getReceipt(uint256 id) external returns(string);

    function getReceiptTransactions(uint256 id) external returns(string);
}
