pragma solidity ^0.4.18;
import "../utils/JsmnSolLib.sol";
import "../utils/strings.sol";
import "./IReceipt.sol";
import "../sop/ISOP.sol";

contract ParamReceipts is IReceipt {
    using strings for *;

    address private sopContractAddress;
    ISOP private iSOP;

    function ParamReceipts(address _address) public{
        sopContractAddress = _address;
        iSOP = ISOP(_address);
    }

    struct Receipt {
        address sellerId;
        address buyerId;
        uint256 date;
        uint256[] receiptProducts;
        uint discountPercentage;
        uint discountAmount;
        uint taxPercentage;
        uint taxAmount;
        uint subTotal;
        uint netTotal;
        Status status;
        Step step;
    }

    struct ReceiptTransaction{
        uint256 blockNumber;
        address initiator;
        Status status;
        Step step;
    }

    struct ReceiptProduct {
        uint256 productId;
        // string productTitle;
        // string categoryName;
        // string brandName;
        uint64 qtyAsked;
        uint64 qtyFulfilled;
        uint256 mPrice;
        uint256 gPrice;
        uint256 taxAmount;
        uint256 prodBarcode;
    }

    struct Product {
        string productTitle;
        string categoryName;
        // uint256 categoryId;
        string brandName;
        // uint256 brandId;
    }

    enum Status { 
        INIT_PROPOSAL,
        CREATE_PO,
        SEND_INVOICE,
        MAKE_PAYMENT,
        CREATE_RECEIPT,
        CANCEL
    }

enum Step {
        INIT_PROPOSAL,
        ACCEPT_PROPOSAL_AND_CREATE_PO,
        REJECT_PROPOSAL,
        SEND_INVOICE,
        PAYMENT_SUCCESS,
        PAYMENT_FAIL,
        CREATE_RECEIPT,
        CANCEL
}

    Receipt[] internal receipts;

    ReceiptTransaction[] internal receiptTransactions;
    
    ReceiptProduct[] internal receiptProducts;

    //All unique products.
    Product[] internal products;
    
    //buyer address -> receipt id's
    mapping (address=>uint256[]) internal buyerReceipts;
    
    //seller address -> receipt id's
    mapping (address=>uint256[]) internal sellerReceipts;

    mapping (uint256=>uint256[]) internal receiptTransactionsMap;

    // barcode -> uint[]
    // mapping (uint256=>uint256[]) internal orderProducts;

    //hash(productname) -> uint256
    mapping (bytes32=>uint256) internal productMap;

    event onStatusChanged(address indexed buyer, address indexed seller, uint256 indexed receiptId, uint256 status, uint256 state);

    function initiateProposal(address _buyerId, string  _productsCSV, uint _discountPercent, uint _taxPercent, uint discountAmount, uint taxAmount, uint subTotal, uint netTotal) external returns(uint256){
        
        uint receiptId = receipts.length;
        uint256[] memory receiptProductIds = parseProductsCSV(_productsCSV);

        Receipt memory receipt = Receipt(msg.sender,_buyerId, now, receiptProductIds, _discountPercent, discountAmount, _taxPercent, taxAmount, subTotal, netTotal, Status.INIT_PROPOSAL, Step.INIT_PROPOSAL);
        receipts.push(receipt);

        initReceiptSOP(receiptId);
        initReceiptTransaction(receiptId);

        onStatusChanged(_buyerId, msg.sender, receiptId, uint(Status.INIT_PROPOSAL), uint(Step.INIT_PROPOSAL));
        return receiptId;
    }

    function initReceiptTransaction(uint256 receiptId) internal{
        ReceiptTransaction memory receiptTransaction = ReceiptTransaction(block.number, msg.sender, receipts[receiptId].status, receipts[receiptId].step);
        uint256 receiptTransId = receiptTransactions.length;
        receiptTransactions.push(receiptTransaction);
        receiptTransactionsMap[receiptId].push(receiptTransId);

        //init SOP with receipt status.
        initReceiptStateSOP(receiptId);
    }

    function acceptProposal(uint256 orderId, address sellerId) external{
        Receipt memory receipt = receipts[orderId];
        
        require(receipts[orderId].buyerId == msg.sender && receipts[orderId].sellerId == sellerId);
        require(receipt.status == Status.INIT_PROPOSAL && receipt.step == Step.INIT_PROPOSAL);

        receipts[orderId].status = Status.CREATE_PO;
        receipts[orderId].step = Step.ACCEPT_PROPOSAL_AND_CREATE_PO;
        initReceiptTransaction(orderId);

        onStatusChanged(msg.sender,sellerId, orderId, uint(Status.CREATE_PO), uint(Step.ACCEPT_PROPOSAL_AND_CREATE_PO));
    }

    function rejectProposal(uint256 orderId, address sellerId) external{
        Receipt memory receipt = receipts[orderId];

        require(receipts[orderId].buyerId == msg.sender && receipts[orderId].sellerId == sellerId);
        require(receipt.status == Status.INIT_PROPOSAL && receipt.step == Step.INIT_PROPOSAL);
        receipts[orderId].status = Status.CANCEL;
        receipts[orderId].step = Step.REJECT_PROPOSAL;
        initReceiptTransaction(orderId);

        onStatusChanged(msg.sender,sellerId, orderId, uint(Status.CANCEL), uint(Step.REJECT_PROPOSAL));
    }

    function sendInvoice(uint256 orderId, address buyerId) external returns(bool){
        Receipt memory receipt = receipts[orderId];

        require(receipts[orderId].buyerId == buyerId && receipts[orderId].sellerId == msg.sender);
        require(receipt.status == Status.CREATE_PO && receipt.step == Step.ACCEPT_PROPOSAL_AND_CREATE_PO);

        receipts[orderId].status = Status.SEND_INVOICE;
        receipts[orderId].step = Step.SEND_INVOICE;

        initReceiptTransaction(orderId);
        onStatusChanged(buyerId, msg.sender, orderId, uint(Status.SEND_INVOICE), uint(Step.SEND_INVOICE));
    }

    function paymentSuccess(uint256 orderId, address sellerId) external returns(bool){
        Receipt memory receipt = receipts[orderId];

        require(receipts[orderId].buyerId == msg.sender && receipts[orderId].sellerId == sellerId);
        require(receipt.status == Status.SEND_INVOICE && receipt.step == Step.SEND_INVOICE);

        receipts[orderId].status = Status.MAKE_PAYMENT;
        receipts[orderId].step = Step.PAYMENT_SUCCESS;

        initReceiptTransaction(orderId);

        onStatusChanged(msg.sender, sellerId,  orderId, uint(Status.MAKE_PAYMENT), uint(Step.PAYMENT_SUCCESS));
    }

    function paymentFail(uint256 orderId, address sellerId) external returns(bool){
        Receipt memory receipt = receipts[orderId];

        require(receipts[orderId].buyerId == msg.sender && receipts[orderId].sellerId == sellerId);
        require((receipt.status == Status.MAKE_PAYMENT && receipt.step == Step.PAYMENT_FAIL) || (receipt.status == Status.SEND_INVOICE && receipt.step == Step.SEND_INVOICE));

        receipts[orderId].status = Status.MAKE_PAYMENT;
        receipts[orderId].step = Step.PAYMENT_FAIL;
        initReceiptTransaction(orderId);
        onStatusChanged(msg.sender, sellerId,  orderId, uint(Status.MAKE_PAYMENT), uint(Step.PAYMENT_FAIL));

    }

    function createReceipt(uint256 orderId, address buyerId) external {
        Receipt memory receipt = receipts[orderId];

        require(receipts[orderId].buyerId == buyerId && receipts[orderId].sellerId == msg.sender);
        require(receipt.status == Status.MAKE_PAYMENT && receipt.step == Step.PAYMENT_SUCCESS);

        receipts[orderId].status = Status.CREATE_RECEIPT;
        receipts[orderId].step = Step.CREATE_RECEIPT;
        
        initReceiptTransaction(orderId);
        onStatusChanged(buyerId, msg.sender, orderId, uint(Status.CREATE_RECEIPT), uint(Step.CREATE_RECEIPT));
    }

    function getTotalReceiptsByStatus(uint status) public returns(uint256){
        Status s = Status(status);
        uint256 len = receipts.length;
        uint count = 0;
        for(uint256 index = 0; index<len; index++){
            if(receipts[index].status == s){
                count++;
            }
        }
        return count;
    }
    
    function initReceiptSOP(uint256 orderId) private {
        Receipt memory receipt = receipts[orderId];
        uint256[] memory receiptProductIds = receipt.receiptProducts;
        string memory receiptId = JsmnSolLib.uintToBytes(orderId);
        iSOP.push("receipt", "rid", receiptId);
        iSOP.push(receiptId, "seller", JsmnSolLib.toAsciiString(receipt.sellerId));
        iSOP.push(receiptId, "buyer", JsmnSolLib.toAsciiString(receipt.buyerId));
        
        string memory receiptInfo;
        Product memory product;
        for(uint index = 0; index < receiptProductIds.length; index++){
            uint256 productIndex = receiptProducts[receiptProductIds[index]].productId;
            product = products[productIndex];
            receiptInfo = append(product.productTitle,",");
            receiptInfo = append(receiptInfo,  JsmnSolLib.uintToBytes(receiptProducts[receiptProductIds[index]].mPrice));
            receiptInfo = append(receiptInfo,  ",");
            receiptInfo = append(receiptInfo,  JsmnSolLib.uintToBytes(receiptProducts[receiptProductIds[index]].taxAmount));
            iSOP.push(receiptId, "productsinfo", receiptInfo);
            iSOP.push(receiptId, "products", product.productTitle);
            iSOP.push(receiptId, "manufacturer",product.brandName);
        }

        receiptInfo = append(JsmnSolLib.uintToBytes(orderId),",");
        receiptInfo = append(receiptInfo,  JsmnSolLib.uintToBytes(receipt.taxAmount));
        receiptInfo = append(receiptInfo,",");
        receiptInfo = append(receiptInfo,  JsmnSolLib.uintToBytes(receipt.subTotal));
        receiptInfo = append(receiptInfo,",");
        receiptInfo = append(receiptInfo,  JsmnSolLib.uintToBytes(receipt.netTotal));
        iSOP.push(receiptId, "rmetainfo", receiptInfo);

    }

    

    function initReceiptStateSOP(uint256 orderId) private {
        string memory receiptId = JsmnSolLib.uintToBytes(orderId);
        receiptId = append("receipt-", receiptId);
        string memory status = JsmnSolLib.uintToBytes(uint(receipts[orderId].status));
        string memory step = JsmnSolLib.uintToBytes(uint(receipts[orderId].step));

        iSOP.push(receiptId, "step", step);
        iSOP.push(receiptId, "status", status);
    }

    function parseProductsCSV(string _productsCSV) internal returns(uint256[]){
        var productsCSVString = _productsCSV.toSlice();
        var delimter = "|".toSlice();
        uint len = productsCSVString.count(delimter) + 1;
        uint256[] memory receiptProductIds = new uint256[](len);
        for(uint256 i = 0; i < len; i++) {
            string memory prodCSV = productsCSVString.split(delimter).toString();
            ReceiptProduct memory receiptProduct = parseProductCSV(prodCSV);
            receiptProduct.prodBarcode = block.number+now+i+1;
            uint256 receiptProductId = receiptProducts.length;
            receiptProducts.push(receiptProduct);
            receiptProductIds[i] = receiptProductId;
        }
        return receiptProductIds;
    }
    
    function parseProductCSV(string _prodCSV) internal returns(ReceiptProduct){
        var _prod = _prodCSV.toSlice();
        var delim = ",".toSlice();
        var maxLen = _prod.count(delim) + 1;
        uint256 productId = 0;
        require(maxLen == 8);
        string memory productTitle = _prod.split(delim).toString();
        string memory categoryName = _prod.split(delim).toString();
        string memory brandName = _prod.split(delim).toString();

        if(productExists(productTitle) == false){
            Product memory _product;
            _product.productTitle = productTitle;
            _product.categoryName = categoryName;
            _product.brandName = brandName;
            productId = products.length;
            productMap[keccak256(productTitle)] = productId;

            iSOP.push("manufacturer","name", brandName);
            iSOP.push(brandName,"product", productTitle);
            
            products.push(_product);
        }else{
            productId = productMap[keccak256(productTitle)];
        }
        ReceiptProduct memory receiptProduct;
        // receiptProduct.productTitle = productTitle;
        // receiptProduct.categoryName = categoryName;
        // receiptProduct.brandName = brandName;
        receiptProduct.productId = productId;
        receiptProduct.qtyAsked = uint64(JsmnSolLib.parseInt(_prod.split(delim).toString()));
        receiptProduct.qtyFulfilled = uint64(JsmnSolLib.parseInt(_prod.split(delim).toString()));
        receiptProduct.mPrice = uint256(JsmnSolLib.parseInt(_prod.split(delim).toString()));
        receiptProduct.gPrice = uint256(JsmnSolLib.parseInt(_prod.split(delim).toString()));
        receiptProduct.taxAmount = uint256(JsmnSolLib.parseInt(_prod.split(delim).toString()));
        return receiptProduct;
    }

    function getReceipt(uint id) external returns(string){
        return toReceiptCSV(id);
    }

    function getReceiptTransactions(uint256 id) external returns(string){
        uint256[] memory trans = receiptTransactionsMap[id];
        string memory transCSV = "";
        for(uint index = 0; index<trans.length; index++){
            transCSV = append(toReceiptTransCSV(trans[index]), "|");
        }
        return transCSV;
    }

    function toReceiptTransCSV(uint256 index) internal returns(string){
        ReceiptTransaction memory transReceipt = receiptTransactions[index];
        string memory receiptTransCSV = JsmnSolLib.toAsciiString(transReceipt.initiator);
        receiptTransCSV = append(receiptTransCSV, ",");
        receiptTransCSV = append(receiptTransCSV, JsmnSolLib.uintToBytes(transReceipt.blockNumber));
        receiptTransCSV = append(receiptTransCSV, ",");
        receiptTransCSV = append(receiptTransCSV, JsmnSolLib.uintToBytes(uint(transReceipt.status)));
        receiptTransCSV = append(receiptTransCSV, ",");
        receiptTransCSV = append(receiptTransCSV, JsmnSolLib.uintToBytes(uint(transReceipt.step)));
        return receiptTransCSV;
    }    

    function toReceiptCSV(uint id) view internal returns(string){
        uint256[] memory receiptProductIds = receipts[id].receiptProducts;
        string memory productsCSV = toReceiptMetaInfo(id);
        productsCSV = append(productsCSV,"\n");
        for(uint index = 0; index < receiptProductIds.length; index++) {
            productsCSV = append(productsCSV, toReceiptProductCSV(receiptProductIds[index]));
            productsCSV = append(productsCSV,"|");
        }
        return productsCSV;
    }


    function toReceiptProductCSV(uint256 receiptProductId) view internal returns(string) {
        ReceiptProduct memory receiptProduct = receiptProducts[receiptProductId];
        string memory receiptProductCSV = toProductCSV(receiptProduct.productId);
        // string memory receiptProductCSV = receiptProduct.productTitle;
        // receiptProductCSV = append(receiptProductCSV, ",");
        // receiptProductCSV = append(receiptProductCSV, receiptProduct.categoryName);
        // receiptProductCSV = append(receiptProductCSV, ",");
        // receiptProductCSV = append(receiptProductCSV, receiptProduct.brandName);

        receiptProductCSV = append(receiptProductCSV, ",");
        receiptProductCSV = append(receiptProductCSV, JsmnSolLib.uintToBytes(receiptProduct.qtyAsked));
        receiptProductCSV = append(receiptProductCSV, ",");
        receiptProductCSV = append(receiptProductCSV, JsmnSolLib.uintToBytes(receiptProduct.qtyFulfilled));
        receiptProductCSV = append(receiptProductCSV, ",");
        receiptProductCSV = append(receiptProductCSV, JsmnSolLib.uintToBytes(receiptProduct.mPrice));
        receiptProductCSV = append(receiptProductCSV, ",");
        receiptProductCSV = append(receiptProductCSV, JsmnSolLib.uintToBytes(receiptProduct.gPrice));
        receiptProductCSV = append(receiptProductCSV, ",");
        receiptProductCSV = append(receiptProductCSV, JsmnSolLib.uintToBytes(receiptProduct.prodBarcode));
        return receiptProductCSV;
    }


    function toReceiptMetaInfo(uint256 id) view internal returns(string){
        string memory metaInfo = "";
        Receipt memory receipt = receipts[id];
        metaInfo = append(JsmnSolLib.uintToBytes(id), ",");
        metaInfo = append(metaInfo, JsmnSolLib.toAsciiString(receipt.sellerId));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo, JsmnSolLib.toAsciiString(receipt.buyerId));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo, JsmnSolLib.uintToBytes(receipt.date));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.discountPercentage));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.discountAmount));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.taxPercentage));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.taxAmount));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.subTotal));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(receipt.netTotal));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(uint256(receipt.status)));
        metaInfo = append(metaInfo,",");
        metaInfo = append(metaInfo,  JsmnSolLib.uintToBytes(uint256(receipt.step)));
        return metaInfo;
    }

    function toProductCSV(uint256 productId) view private returns(string){
        Product memory product = products[productId];
        string memory productCSV = append(productCSV, product.productTitle);//append(JsmnSolLib.uintToBytes(productId), ",");
        // productCSV = append(productCSV, product.productTitle);
        productCSV = append(productCSV, ",");
        productCSV = append(productCSV, product.categoryName);
        productCSV = append(productCSV, ",");
        // productCSV = append(productCSV, JsmnSolLib.uintToBytes(product.categoryId));
        // productCSV = append(productCSV, ",");
        productCSV = append(productCSV, product.brandName);
        // productCSV = append(productCSV, ",");
        // productCSV = append(productCSV, JsmnSolLib.uintToBytes(product.brandId));
        return productCSV;
    }

    function append(string mainStr, string str) pure internal returns(string){
       return mainStr.toSlice().concat(str.toSlice());
    }

    function productExists(string productTitle) view private returns(bool){
        if(products.length==0){
            return false;
        }
        bytes32 hashTitle = keccak256(productTitle);
        uint256 productId = productMap[hashTitle];
        return ((productId != 0) || (productId == 0 && keccak256(products[0].productTitle) == hashTitle));
    }
}