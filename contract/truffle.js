module.exports = {
  networks: {
    hotspot: {
      host: "192.168.43.49",
      port: 8545,
      network_id: 2564017, // Match any network id
      eip155Block:0,
      chainId:2564017,
      gas: 5852388
    },
    iit: {
      host: "10.194.223.168",
      port: 8545,
      network_id: 2564017,
      eip155Block:0,
      chainId:2564017,
      gas: 5852388
    },
    localhost: {
      host: "192.168.0.9",
      port: 8545,
      network_id: 2564017, // Match any network id
      eip155Block:0,
      chainId:2564017,
      gas: 5852388
    },
  },

  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
};
