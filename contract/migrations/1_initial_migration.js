var sopContract = artifacts.require("./sop/SOP.sol");
var paramReceipt = artifacts.require("./receipt/ParamReceipts.sol");

module.exports = function(deployer) {

  deployer.deploy(sopContract).then(function(_instance){
    deployer.deploy(paramReceipt, sopContract.address);
  });

};
