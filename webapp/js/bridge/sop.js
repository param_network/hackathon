function initSOP(){
    sopABI = web3.eth.contract(sop.abi);
    sopContract = sopABI.at(SOP_CONTRACT_ADDRESS);//
  }
initSOP();
function pull(s,o, callback){
    sopContract.pull.call(s, o, function(e, data){
        if(e){
            callback(e, null);
            return;
        }
        callback(e, toPullJSON(data));
    });
}

function toPullJSON(data){
    var manufacturers = data.split('`');
    var jsonArray = [];
    for(var index=0;index<manufacturers.length;index++){
        if(manufacturers[index]){
            jsonArray.push(manufacturers[index]);
        }
    }
    return jsonArray;
}