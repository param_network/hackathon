function ParamReceipt(){

}
ParamReceipt.getBuyerOrdersByStatusMetaInfo = function(buyerId,step,callback){
    debugger;
    paramReceiptsContract.getBuyerOrdersByStatusMetaInfo.call(buyerId,step, function(error, result){
        if(error){
            callback(error, result);
            return;
        }
    });
}
// ParamReceipt.getBuyerOrdersByStatusMetaInfo = function(buyerId, step, callback, topElemenets){
//     if(typeof topElemenets == undefined || !topElemenets){
//         topElemenets = 8;
//     }
//      paramReceiptsContract.getBuyerOrdersByStatusMetaInfo.call(buyerId,step,function(err, steps){
//         if(err){
//             var ordersJSON = {"total": 0, "receipts":[jsonArray]};
//             callback(err, ordersJSON);
//             return;
//         }
//         var stepsArray = steps.split("\n");
//         var jsonArray = [];

//         for(var index=0;index<stepsArray.length;index++){
//             if(!stepsArray[index]){
//                 continue;
//             }
//             jsonArray.push(ParamReceipt.receiptInfoToJSON(stepsArray[index]));
//         }
//         var totalReceipts = jsonArray.length;
//         if(jsonArray.length > topElemenets){
//             jsonArray = jsonArray.slice(0, Math.max(jsonArray.length - topElemenets, topElemenets));
//         }
//         var ordersJSON = {"total": totalReceipts, "receipts":jsonArray};
//         callback(err, ordersJSON);
//     });
// }
ParamReceipt.sendInvoice = function(rid, buyerID, callback){
    unlockSellerAccount();
    paramReceiptsContract.sendInvoice.estimateGas(rid, buyerID, function(error, _gas){
        if(error){
            callback(error, null);
            return;
        }
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.sendInvoice.sendTransaction(rid, buyerID, {gas:_gas,from: getSellerId()}, function(err,data){
            if(err){
                callback(err, null);
                return;
            }
            callback(null,data);
        });
    });
}
ParamReceipt.createReceipt = function(rId, buyerID, callback){
    unlockSellerAccount();
    paramReceiptsContract.createReceipt.estimateGas(rId, buyerID, function(err, _gas){
        if(err){
            callback(err, null)
            return;
        }
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.createReceipt.sendTransaction(rId, buyerID, {gas:_gas,from: getSellerId()}, function(err,data){
            if(err){
                callback(err, null);
                return;
            }
            callback(null, data);             
        });
    });
}

ParamReceipt.getOrderInfo = function(index, callback){
    paramReceiptsContract.getReceipt.call(index, function(error, result){
        if(error){
            callback(error, result);
            return;
        }
        var receipt = ParamReceipt.receiptToJSON(result);
        receipt.rId = index;
        callback(error, receipt);
    });
}

ParamReceipt.getTransactionsInfo = function(rid, callback){
    debugger;
    paramReceiptsContract.getReceiptTransactions.call(rid, function(err, result){
        if(err){
            callback(err, result);
            return;
        }
        var result = result.split('|');
        var transactionsJSON = {};
        var transactions = [];
        for(var index=0;index<result.length;index++){
            if(!result[index]){
                continue;
            }
            transactions.push(ParamReceipt.transactionsInfoToJSON(result[index]))
        }
        transactionsJSON.transactions = transactions;
        transactionsJSON.rId = rid;
        callback(err, transactionsJSON);
    });
}
ParamReceipt.receiptToJSON = function(result){
    var metaInfo = result.split('\n')[0];
    result = result.split('\n')[1].split("|");
    var metaInfoJSON = ParamReceipt.receiptInfoToJSON(metaInfo);
    var receipt = {};
    var products = [];
    for(var index=0;index<result.length;index++){
        var tableRow = result[index].split(',');
        if(tableRow.length==1){
            continue;
        }
        var productInfo = {};
        productInfo['pName'] =  tableRow[0];
        productInfo['cName'] =  tableRow[1];
        productInfo['bName'] =  tableRow[2];
        productInfo['qtyAsked'] =  tableRow[3];
        productInfo['qtyFulfilled'] =  tableRow[4];
        productInfo['mPrice'] =  tableRow[5];
        productInfo['gPrice'] =  tableRow[6];
        productInfo['prodBarcode'] =  tableRow[7];
        products.push(productInfo);
    }
    receipt.products = products;
    receipt.step = metaInfoJSON.step;
    receipt.stepStr = getStep(metaInfoJSON.step);
    receipt.status = metaInfoJSON.status;
    receipt.billInfo = metaInfoJSON; 
    return receipt;
}

ParamReceipt.receiptInfoToJSON = function(receiptInfo){
    var receiptMetaInfoArray = receiptInfo.split(',');
    var metaJSON = {};
    metaJSON['id'] = receiptMetaInfoArray[0];
    metaJSON['sellerId'] = "0x"+receiptMetaInfoArray[1];
    metaJSON['buyerId'] = "0x"+receiptMetaInfoArray[2];
    metaJSON['date'] = receiptMetaInfoArray[3];
    metaJSON['discountPercent'] = getPercentage(receiptMetaInfoArray[4]);
    metaJSON['discountAmount'] = getSum(receiptMetaInfoArray[5]);
    metaJSON['taxPercent'] = getPercentage(receiptMetaInfoArray[6]);
    metaJSON['taxAmount'] = getSum(receiptMetaInfoArray[7]);
    metaJSON['subTotal'] = getSum(receiptMetaInfoArray[8]);
    metaJSON['netTotal'] = getSum(receiptMetaInfoArray[9]);
    metaJSON['step'] = parseInt(receiptMetaInfoArray[10]);
    metaJSON['status'] = parseInt(receiptMetaInfoArray[11]);
    metaJSON['blockNumber'] = parseInt(receiptMetaInfoArray[12]);
    metaJSON['currentDate'] = parseInt(receiptMetaInfoArray[13]);
    return metaJSON;
}
ParamReceipt.transactionsInfoToJSON = function(transactionStr){
    var transInfoArray = transactionStr.split(',');
    var transJSON = {};
    transJSON['init'] = "0x"+transInfoArray[0];
    transJSON['txnId'] = transInfoArray[1];
    transJSON['step'] = parseInt(transInfoArray[2]);
    transJSON['status'] = parseInt(transInfoArray[3]);
    var stepStr = getStep(transJSON.step);
    var statusMsg= "";
    if(transJSON.status==4){
        statusMsg = " success";
    }else if(transJSON.status==5){
        statusMsg = " fail"
    }
    stepStr+=statusMsg;
    transJSON['stepStr'] = stepStr;
    transJSON['statusStr'] = statusMsg;
    return transJSON;
}

ParamReceipt.acceptProposal =function(id, sellerID, callback){
    debugger;
    unlockBuyerAccount();
    paramReceiptsContract.acceptProposal.estimateGas(id,sellerID, function(err, _gas){
        if(err){
            callback(err,null)
            return;
        }
        debugger;            
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.acceptProposal.sendTransaction(id,sellerID, {gas:_gas, from: getBuyerId()}, function(err,data){
            if(err){
                callback(err,null)
                return;
            }
            callback(null, data);
        });
    });
}
ParamReceipt.rejectProposal = function(id, sellerID, callback){
    unlockAccount();
    paramReceiptsContract.rejectProposal.estimateGas(id, sellerID, function(err, _gas){
        if(err){
            callback(err)
            return;
        }
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.rejectProposal.sendTransaction(id, sellerID, {gas:_gas,from: getBuyerId()}, function(err,data){
            if(err){
                callback(err)
            }
            callback(err, data);
        });
    });
}

ParamReceipt.paymentFail = function(id,sellerID,callback){
    unlockAccount();
    paramReceiptsContract.makePaymentFailure.estimateGas(id,sellerID,function(err, _gas){
            if(err){
                callback(err)
                return;
            }
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.makePaymentFailure.sendTransaction(id,sellerID,{gas:_gas,from: getBuyerId()}, function(err,data){
            if(err){
                        callback(err)
            }
            callback(err, data);
        });
    });
}

ParamReceipt.paymentInit= function(id,sellerID,callback){
    unlockAccount();
    paramReceiptsContract.makePaymentInit.estimateGas(id, sellerID, function(err, _gas){
            if(err){
                callback(err)
                return;
            }
            _gas = parseInt(_gas*1.3);
            paramReceiptsContract.makePaymentInit.sendTransaction(id, sellerID, {gas:_gas,from: getBuyerId()}, function(err,data){
               if(err){
                    callback(err)
                }
                callback(err, data);
            });    
    });
}

ParamReceipt.makePaymentSuccess= function(id,sellerID,callback){
    unlockBuyerAccount();
    paramReceiptsContract.paymentSuccess.estimateGas(id,sellerID,function(err, _gas){
            if(err){
                callback(err)
                return;
            }
        _gas = parseInt(_gas*1.3);
        paramReceiptsContract.paymentSuccess.sendTransaction(id, sellerID, {gas:_gas,from: getBuyerId()}, function(err,data){
            if(err){
                callback(err)
            }
            callback(err, data);
        });
    });
}


