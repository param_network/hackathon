var graphData = [];
var linkInfo = [];
var metaInfo = [];

function clearGraphData(){
    graphData = [];
    linkInfo = [];
    metaInfo = [];
}

function getProductNode(node, groupName, callback){
    pull(node,"product", function(e, manufacturers){
        if(e){
            return;
        }
        var index = metaInfo.indexOf(node+groupName);
        if (index > -1) {
            metaInfo.splice(index, 1);
        }
        addNode(groupName, trimString(node), 75);
        addLink(trimString(node), groupName, 1);
        for(var index=0;index<manufacturers.length;index++){
            addNode("Product", trimString(manufacturers[index]),150);
            addLink(trimString(manufacturers[index]), node);
        }
        invokeCallback({
            onSuccess: function(){
                exploreReceipts(callback);
            }
        });
    });
}

function exploreReceipts(callback){
    addNode("receipts", "Receipts", 300);
    exploreNetworkReceipts(callback);
}
function exploreSellerReceipts(callback){
    pull(userInfo.userId.substring(2),"seller", function(e, receipts){
        if(e){
            invokeCallback(callback);
            return;
        }
        if(SHOW_BUYER_SELLER_GRAPH == MAIN_GRAPH)
            exploreBuyerReceipts(callback);
        toReceipts(receipts,callback);
    });
}
function exploreBuyerReceipts(callback){
    pull(userInfo.userId.substring(2),"buyer", function(e, receipts){
        if(e){
            invokeCallback(callback);
            return;
        }
        toReceipts(receipts, callback);
    });
}
function toReceipts(receipts, callback){
    for(var index=0;index<receipts.length;index++){
        addNode("receipt", receipts[index], 150);
        addLink(receipts[index], "Receipts");
        exploreReceiptById(receipts[index], callback);
        exploreReceiptStepById(receipts[index], callback);
    }
    invokeCallback(callback);
}
function exploreNetworkReceipts(callback){
    pull("receipt","rid", function(e, receipts){
        if(e){
            invokeCallback(callback);
            return;
        }
        for(var index=0;index<receipts.length;index++){
            addNode("receipt", receipts[index], 150, "./details.html?id="+receipts[index]+"");
            addLink(receipts[index], "Receipts");
            exploreReceiptById(receipts[index], callback);
            exploreReceiptStepById(receipts[index], callback);
        }
        invokeCallback(callback);
    });
}

function exploreReceiptById(rId, callback){
    metaInfo.push(rId+"products");
    pull(rId,"products", function(e, products){
        if(e){
            invokeCallback(callback);
            return;
        }
        var index = metaInfo.indexOf(rId+"products");
        if (index > -1) {
            metaInfo.splice(index, 1);
        }
        for(var index=0;index<products.length;index++){
            addNode("Receipts", products[index], 75);
            addLink(products[index], rId);
            exploreReceiptyTag(rId, "buyer", callback)
            exploreReceiptyTag(rId, "seller", callback)
        }
        invokeCallback(callback);
    });
}
function exploreReceiptStepById(rId, callback){
    metaInfo.push(rId+"status");
    pull("receipt-"+rId,"status", function(e, step){
        if(e){
            invokeCallback(callback);
            return;
        }
        debugger;
        var index = metaInfo.indexOf(rId+"status");
        if (index > -1) {
            metaInfo.splice(index, 1);
        }
        step = step[step.length-1];
        addLink(getStateStr(step), rId);
        invokeCallback(callback);
    });
}
function exploreReceiptyTag(rId, tag, callback){
    metaInfo.push(rId+tag);
    addNode("tag", tag, 30);
    pull(rId, tag, function(e, users){
        if(e){
            invokeCallback(callback);
            return;
        }
        var index = metaInfo.indexOf(rId+tag);
        if (index > -1) {
            metaInfo.splice(index, 1);
        }
        for(var index=0;index<users.length;index++){
            var user = getShortUserId(users[index]);
            addNode("users", user, 45);
            addNode("rId", rId+"-"+tag, 45);
            // addLink(tag, user);
            addLink(rId, rId+"-"+tag);
            addLink(rId+"-"+tag, user);
            addLink(tag, rId+"-"+tag);
        }
        invokeCallback(callback);
    });
}

function invokeCallback(callback){
    if(metaInfo.length==0){
        callback.onSuccess(graphData,linkInfo, getOptions());
        // graphData = linkInfo = [];
    }
}

function getOptions(){
    var options = {"radius":2.5,"fontSize":9,"labelFontSize":9,"gravity":0.1,"height":800,"nodeFocusColor":"black","nodeFocusRadius":25,"nodeFocus":true,"linkDistance":150,"charge":-220,"nodeResize":"count","nodeLabel":"label","linkName":"tag"};
    return options;
}

function getGraph(callback) {
    pull("manufacturer","name", function(e, manufacturers){
        if(e){
            return;
        }
        // var manufacturers = data.split('`');
        addNode("State", "State", 200)
        for(var index=0;index<STATES.length;index++){
            // if(index==3){
            //     addNode("State", STATES[index]+" SUCCESS",  80);
            //     addLink("State", STATES[index]+" SUCCESS");
            //     addNode("State", STATES[index]+" FAIL",  80);
            //     addLink("State", STATES[index]+" FAIL");
            // }else{
                addNode("State", STATES[index],  80);
                addLink("State", STATES[index]);
            // }
        }
        // var manufacturers = data.split('`');
        addNode("Manufacturers", "Manufacturers", 200)
        for(var index=0;index<manufacturers.length;index++){
            metaInfo.push(manufacturers[index]+"Manufacturers");
            getProductNode(manufacturers[index],"Manufacturers", callback);
        }
    });
}

function addNode(groupName, name, count, url){
    if(!count){
        count = 1;
    }
    if(isNodeExists(name)){
        return;
    }
    graphData.push({"count":count,"group":groupName,"label":name,"shortName":name, "url":url});
}

function isNodeExists(name){
    for(var index=0;index<graphData.length;index++){
        if(graphData[index].shortName==name){
            return true;
        }
    }
    return false;
}

function editNode(name){

}

function deleteNode(name){

}
function addLink(node, groupName){
    var sourceNode = targetNode = -1;
    for(var index=0;index<graphData.length;index++){
        if(graphData[index].shortName==groupName){
            sourceNode = index;
        }
        if(graphData[index].shortName==node){
            targetNode = index;
        }

        if(sourceNode!=-1 && targetNode!=-1){
            break;
        }
    }
    if(sourceNode!=-1 && targetNode!=-1){
        linkInfo.push({"source":sourceNode,"target":targetNode,"depth":9,"count":12});
    }
}

// function onTransactionUpdateEvent(){
//     window.location.reload();
// }
var STATES = ['PROPOSAL','PO','INVOICE','PAYMENT','RECEIPT','CANCEL'];
function getStateStr(state){
    return STATES[parseInt(state)];
}

function getStatusStr(status){
    return STATES[state];

}