
startWeb3();
function startWeb3(){
    var userOption = getOption();
    if(userOption ==-1){
        return;
    }
    var provider;;
    if(userOption == 1){
        provider = new Web3.providers.HttpProvider(getValue())
    }else if(userOption == 0){
        if(window.web3 == undefined){
            //Meta mask...
            web3Error({errcode:1})
            return;
        }
        provider = web3.currentProvider;
    }
    web3 = new Web3(provider);
    web3.version.getNetwork(function(err, networkId){
        switch(networkId){
            case NETWORK_ID:
                if(isNodeLive(userOption == 0)){
                    onWeb3Create();
                    return;
                }
                //No node avilable.
                web3Error({errcode:3})
                break;
            default:
                web3Error({errcode:2})
        }
    });
}
function onWeb3Create(){
    debugger;
     if(paramReceiptABI != undefined){
        var paramReceiptsContractABI = web3.eth.contract(paramReceiptABI.abi);
        paramReceiptsContract = paramReceiptsContractABI.at(PARAM_CONTRACT_ADDRESS);
        initEvents();
    }

    if(typeof onPostWeb3Init == "function"){
        onPostWeb3Init();
    }
    
    if(typeof onUpdateHeader == "function"){
        onUpdateHeader();
    }
    
}
function isNodeLive(isMetaMask){
    if(isMetaMask)
        return web3.currentProvider.publicConfigStore._state.networkVersion !== "loading";
    return web3.isConnected(); 
}

function web3Error(json){
    var location = "";
    if(!window.location.href.includes("/pages/")){
        location="./pages/";
    }
    window.location = location+"error.html?id="+json.errcode+""
    // if(typeof onWeb3Error == 'function'){

    // }
}
function initEvents(){
    paramReceiptsContract.onStatusChanged({seller:getSellerId()}, function(error, result){
        if(error){
            console.error(error);
            return;
        }
        debugger;
        if(typeof onStatusUpdate !== 'function'){
            console.log("onStatusUpdateInfo, ",result);
            return;
        }
        onStatusUpdate(result);
    });

}
function trimString(str){
    if(str.length>26)
        return str.substring(0,20)+"...";
    return str;
}
function getShortUserId(userId){
    if(!userId){
        userId = "";
    }
    userId = userId.toUpperCase();
    if(!userId.startsWith("0X")){
        userId = "0X"+userId;
    }
    return trimString(userId);
}

