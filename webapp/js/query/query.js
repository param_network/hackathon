initStats();
function initStats(){
    loadTab();
    sopContract.pull.call("receipt","rid", function(e, data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        var receiptIds = toPullJSON(data)
        // getSellerDetails(receiptIds);
        // getBuyerDetails(receiptIds);
        getPendingInvoice(receiptIds);
        // userTarget(receiptIds);
    });
        
}
function loadTab(){
    // showLoading();
    $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
    $('.tab ul.tabs li a').click(function (g) { 
        var tab = $(this).closest('.tab'), 
            index = $(this).closest('li').index();
        tab.find('ul.tabs > li').removeClass('current');
        $(this).closest('li').addClass('current');
        tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
        tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
        g.preventDefault();
    } );
}
function getBuyerDetails(receiptIds){
    if(receiptIds.length==0){
        return;
    }
    var resultJSON = [];
    for(var index =0;index<receiptIds.length;index++){
        updateBuyerJSON(receiptIds[index], resultJSON);    
    }    
}
function updateBuyerJSON(receiptId, resultJSON){
    sopContract.pull.call(receiptId,"products",function(e,data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        prodNames = toPullJSON(data);
        for(var j=0;j<prodNames.length;j++){
            prodName = prodNames[j];
            updateBuyerProductJSON(receiptId,prodName, resultJSON)
        }
    });
}

function updateBuyerProductJSON(receiptId, prodName, resultJSON){
    sopContract.pull.call(receiptId,"productsinfo", function(e, data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        metaInfo = toReceiptProductsJSON(toPullJSON(data)[0]);
        debugger;
        if(resultJSON[prodName]){
            metaInfo.price = metaInfo.price + resultJSON[prodName].price;
            metaInfo.tax = metaInfo.tax + resultJSON[prodName].tax;
            metaInfo.count = resultJSON[prodName].count+1; 
            resultJSON[prodName] = metaInfo;
            updateBuyerTable(resultJSON);
            return;
        }
        resultJSON[prodName] = metaInfo;
        updateBuyerTable(resultJSON);
    });
}

function  toReceiptProductsJSON(metaInfo){
    var receiptInfo = metaInfo.split(",");;
    var receiptProductsJSON = {"name":receiptInfo[0], "price": parseInt(receiptInfo[1]), count: 1, tax:parseInt(receiptInfo[2])};
    return receiptProductsJSON;
}
function getSellerDetails(receiptIds){
    if(receiptIds.length == 0){
        return;
    }
    var resultJSON = [];
    for(var index=0;index<receiptIds.length;index++){
        var receiptId = String(receiptIds[index]);
        updateSellerJSON(receiptId, resultJSON);
    }
}

function updateSellerJSON(receiptId, resultJSON){
    sopContract.pull.call(receiptId,"seller", function(e, seller){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        seller = toPullJSON(seller)[0];
        sopContract.pull.call(receiptId,"rmetainfo", function(e, metaInfo){
            if(e){
                showErrorAlert("Unable to get the data.Please try again");
                return;
            }
            metaInfo = toPullJSON(metaInfo);
            temp = metaInfo[0].split(",");
            if(resultJSON[seller]){
                metaInfo.revenue = getSum(temp[2]) + resultJSON[seller].revenue;
                metaInfo.tax =  getSum(temp[1]) + resultJSON[seller].tax;
                metaInfo.count = resultJSON[seller].count+1;
                resultJSON[seller] = metaInfo;
                updateSellerTable(resultJSON);
                return;
            }
            resultJSON[seller] = {"revenue":getSum(temp[2]),"tax":getSum(temp[1]), "count":1};
            updateSellerTable(resultJSON);
        });
    });

}

function userTarget(receiptIds){
    if(receiptIds.length == 0){
        return;
    }
    var resultJSON = [];
    var choosedName = "Canon EOS 1500D 24.1MP";
    for(var index=0;index<receiptIds.length;index++){
        var receiptId = String(receiptIds[index]);
        sopContract.pull.call(receiptId,"products", function(e, data){
            if(e){
                showErrorAlert("Unable to get the data.Please try again");
                return;
            }
            var prodNames = toPullJSON(data);
            for(var j=0;j<prodNames.length;j++){
                if(choosedName == prodNames[j]) {
                    sopContract.pull.call(receiptId,"buyer", function(e, data){
                        if(e){
                            showErrorAlert("Unable to get the data.Please try again");
                            return;
                        }
                        buyer = toPullJSON(data);
                        resultJSON.push(buyer);
                        updateUserTargetTable(resultJSON);
                    });
                }
            }
        });
    }
}

function getPendingInvoice(receiptIds){
    debugger;
    if(receiptIds.length == 0){
        return;
    }
    var resultJSON = [];
    for(var index=0;index<receiptIds.length;index++){
        var receiptId = String(receiptIds[index]);
        updatePendingInvoiceJSON(receiptId, resultJSON);
    }
}

function updatePendingInvoiceJSON(receiptId, resultJSON){
    sopContract.pull.call('receipt-'+receiptId,"status", function(e, data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        var receiptStatus = toPullJSON(data);
        if(receiptStatus.length!=0 && receiptStatus[receiptStatus.length-1] == "2"){
            sopContract.pull.call(receiptId,"rmetainfo", function(e, metaInfo){
                if(e){
                    showErrorAlert("Unable to get the data.Please try again");
                    return;
                }
                metainfo = toPullJSON(metaInfo);
                getInvoiceBuyer(receiptId,metainfo,resultJSON);
            });
        }
    }); 

}
function getInvoiceBuyer(receiptId,metaInfo,resultJSON){
    debugger;
    sopContract.pull.call(receiptId,"buyer", function(e, data){
            if(e){
                showErrorAlert("Unable to get the data.Please try again");
                return;
            }
            resultJSON.push({"amount":metaInfo[0].split(",")[3],"buyer":toPullJSON(data)});
            console.log("buyer result"+JSON.stringify(resultJSON));
            updatePendingTable(resultJSON);
    });
}
function showLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "block";
      $("body").find("*").attr("disabled", "disabled");
      $("body").find("a").click(function (e) { e.preventDefault(); });
      document.body.style.backgroundColor="#f4f4f4";
      document.body.style.opacity=0.5;
}
function hideLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "none";
      $("body").find("*").removeAttr("disabled");
      $("body").find("a").unbind("click");  
      document.body.style.backgroundColor="transparent";
      document.body.style.opacity=1;  
}
function updateBuyerTable(resultJSON){
    $("#stats2").empty();
    for(var key in resultJSON){
        tr = '<tr  class="center">'+
                    '<td>'+key +'</td>'+
                    '<td>'+resultJSON[key].count+'</td>'+
                    '<td>'+getSum(resultJSON[key].price) +'</td>'+
                    '<td>'+getSum(resultJSON[key].tax)+'</td>'+
                '</tr>'
        $("#stats2").append(tr);
    }
}
function updateSellerTable(resultJSON){
    $("#stats1").empty();
    for(var key in resultJSON){
        tr = '<tr  class="center">'+
                    '<td>'+key+'</td>'+
                    '<td>'+resultJSON[key].count +'</td>'+
                    '<td>'+resultJSON[key].revenue +'</td>'+
                    '<td>'+resultJSON[key].tax +'</td>'+
              '</tr>'
        $("#stats1").append(tr);
   }
    
}
function updateUserTargetTable(resultJSON){
    $("#stats3").empty();
    for(var key in resultJSON){
        tr = '<tr  class="center">'+
                    '<td>'+resultJSON[key] +'</td>'+
             '</tr>'
        $("#stats3").append(tr);
    }
}
function updatePendingTable(resultJSON){
    $("#stats4").empty();
    for(var key in resultJSON){
        tr = '<tr  class="center">'+
                    '<td>'+resultJSON[key].buyer +'</td>'+
                    '<td>'+getSum(resultJSON[key].amount) +'</td>'+
             '</tr>'
        $("#stats4").append(tr);
    }
   
}
function reset(){
    $("#stats1").empty();
    $("#stats2").empty();
    $("#stats3").empty();
    $("#stats4").empty();
}
function onStatusUpdate(result){
        reset();
        initStats(); 
}