var rid = "";
var txHash = "";
function onPostWeb3Init() {
    showLoading();    
    counter = 1;
    loadData();
};
function loadData(){
    rid = parseInt(getUrlParameter("id"));
    ParamReceipt.getTransactionsInfo(rid,function(e, transactions){
        displayActionButtonUI(transactions.transactions);
        $("#receiptId").text(rid);
        hideLoading();
    });
}

function displayActionButtonUI(transactions){
    debugger;
    var len = transactions.length;
    var trans = transactions[transactions.length-1];
    var step = transactions[transactions.length-1].step;
    var _location ='../refimages/state00'+(step+1)+'.png'

    var state = String(trans.stepStr).toLowerCase();
    $("#state").text(state);
    $("#block").text(trans.txnId);
    $("#block").text(trans.txnId);
    $("#timelineImg").attr("src", _location);

    for(var index=0;index<5; index++){
        $("#"+index).css("display","none");
    }
    $("#"+(step+1)).css("display","");
}

function paymentSuccess(){
    showLoading();
    ParamReceipt.makePaymentSuccess(rid, getBuyerId(), function(e, tx){
        debugger;
        txHash = tx;
    });
}

function acceptProposal(){
    showLoading();
    ParamReceipt.acceptProposal(rid, getSellerId(), function(e, tx){
        txHash = tx;
    });
}
function onInvoiceClicked(){
    showLoading();
    ParamReceipt.sendInvoice(rid, getSellerId(), function(e, tx){
        txHash = tx;
    });
}

function onReceiptClicked(){
    showLoading();
    ParamReceipt.createReceipt(rid, getSellerId(), function(e, tx){
        txHash = tx;
    });
}

function getActionButtons(status, step, blockNumber){
    sellerID=receipt.billInfo.sellerId;
    showRemainTimline(parseInt(step),parseInt(blockNumber));

    if(status==0 && step!=5){
        $("#accept").attr('value', "Accept Proposal");
        $("#accept").click(acceptProposal);
        $("#reject").attr('value', "Cancel");
        $("#reject").click(rejectProposal);
    }
    else if((status== 5 && step==3) ||(status == 3 && step==2)){
        $("#accept").attr('value', "Payment Success");
        $("#accept").click(makePaymentSuccess);
        $("#reject").attr('value', "Payment Failure");
        $("#reject").click(makePaymentFailure);
    }
}
function showLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "block";
      $("body").find("*").attr("disabled", "disabled");
      $("body").find("a").click(function (e) { e.preventDefault(); });
      document.body.style.backgroundColor="#f4f4f4";
      document.body.style.opacity=0.5;
}
function hideLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "none";
      $("body").find("*").removeAttr("disabled");
      $("body").find("a").unbind("click");  
      document.body.style.backgroundColor="transparent";
      document.body.style.opacity=1;  
}

function onStatusUpdate(result){
    if(typeof displayNotification != "undefined"){
        displayNotification(result,"timeline.html");
    }
    
    if(typeof txHash!=="undefined" && txHash !==null && txHash==result.transactionHash){
        hideLoading();
        txHash = undefined;
        loadData();
        // window.location = "./timeline.html?id="+result.args.receiptId.toString();
    }
}