// transInfo
function getTransactionInfoByStep(){
    var step=parseInt(getUrlParameter("step"));
    var resultJSON = [];
    sopContract.pull.call("receipt","rid", function(e, data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        var receiptIds = toPullJSON(data);
        for(var index =0;index<receiptIds.length;index++){
            getTransInfoByStatus(step,receiptIds[index], resultJSON);    
        }  
    });
}
function getTransInfoByStatus(step,receiptId,resultJSON){
    debugger;
        sopContract.pull.call('receipt-'+receiptId,"status", function(e, data){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        var receiptStatus = toPullJSON(data);
        if(receiptStatus.length!=0 && receiptStatus[receiptStatus.length-1] == step){
            sopContract.pull.call(receiptId,"rmetainfo", function(e, metaInfo){
                if(e){
                    showErrorAlert("Unable to get the data.Please try again");
                    return;
                }
                metainfo = toPullJSON(metaInfo);
                resultJSON.push({"tax":metainfo[0].split(",")[1],"receiptId":metainfo[0].split(",")[0],"netTotal":metainfo[0].split(",")[3]});
                updateTransInfoTable(resultJSON);
            });
            return;
        }
       
    }); 
    // console.log(resultJSON);
}
function updateTransInfoTable(resultJSON){
    $("#selectedStepInfo").empty();
    for(var key in resultJSON){
        tr = '<tr  class="center" onclick="redirectToDetails('+resultJSON[key].receiptId +')">'+
                    '<td>'+resultJSON[key].receiptId +'</td>'+
                    '<td>'+getSum(resultJSON[key].tax) +'</td>'+
                    '<td>'+getSum(resultJSON[key].netTotal) +'</td>'+
             '</tr>'
        $("#selectedStepInfo").append(tr);
    }
}
function redirectToDetails(rid){
    window.location = "./details.html?id="+rid+""
}
// receipt details screen
function onPostWeb3Init(){
    debugger;
    var receiptId = parseInt(getUrlParameter("id"));
    ParamReceipt.getOrderInfo(receiptId,function(e, transactions){
        if(e){
            showErrorAlert("Unable to get the data.Please try again");
            return;
        }
        $("#buyerid").val(transactions.billInfo.buyerId);
        $("#subtotal").text(transactions.billInfo.subTotal);
        $("#discountPer").val(transactions.billInfo.discountPercent);
        $("#discountAmount").text(transactions.billInfo.discountAmount);
        $("#taxPer").val(transactions.billInfo.taxPercent);
        $("#taxAmount").text(transactions.billInfo.taxAmount);
        $("#nettotal").text(transactions.billInfo.netTotal);
        updateProductTable(transactions);
    });  
}
function updateProductTable(transactions){
    for(var key in transactions.products){
        var tr = '<tr>'+
                        '<td>'+transactions.products[key].pName+'</td>'+
                        '<td>'+transactions.products[key].cName+'</td>'+
                        '<td>'+transactions.products[key].qtyAsked+'</td>'+
                        '<td>'+getSum(transactions.products[key].mPrice)+'</td>'+
                 '</tr>'        
    }
    $("#productTableBody").append(tr);
}
