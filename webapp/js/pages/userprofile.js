function initSellerProfileTable(){
    for(var index=0;index<NETWORK_USERS_INFO.length; index++){
        var tableList = '<li class="list-group-item">\
            <div class="custom-control custom-checkbox">\
                <div class="row">\
                    <div class="col-md-6 col-xs-6">\
                        <label class="custom-control-label no-margin" for="defaultUnchecked">Account '+ (index+1)+ '</label>\
                        <p class="no-margin">'+NETWORK_USERS_INFO[index].user+'</p>\
                    </div>\
                    <div class="col-md-6 col-xs-6">\
                        <div class="form-group right usercheckbox">\
                            <input type="radio" name="seller" id="'+NETWORK_USERS_INFO[index].user+'" autocomplete="off" />\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </li>';
        $("#sellerProfileUL").append(tableList);
    }
}
function initBuyerProfileTable(){
    for(var index=0;index<NETWORK_USERS_INFO.length; index++){
        var tableList = '<li class="list-group-item" onclick="updateBuyerInfo('+index+')">\
            <div class="custom-control custom-checkbox">\
                <div class="row">\
                    <div class="col-md-6 col-xs-6">\
                        <label class="custom-control-label no-margin" for="defaultUnchecked">Account '+ (index+1)+ '</label>\
                        <p class="no-margin">'+NETWORK_USERS_INFO[index].user+'</p>\
                    </div>\
                    <div class="col-md-6 col-xs-6">\
                        <div class="form-group right usercheckbox">\
                            <input type="radio" name="buyer" id="'+NETWORK_USERS_INFO[index].user+'" autocomplete="off" />\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </li>';
        $("#buyerProfileUL").append(tableList);
    }
}

function updateSellerInfo(index){
    // localStorage.buyerId = NETWORK_USERS_INFO[index].user;
    localStorage.sellerId = index;
    // window.location = 'proposal.html';
}
function updateBuyerInfo(index){
    localStorage.buyerId = index;
    // localStorage.sellerId = NETWORK_USERS_INFO[index].user;
}
function selectionComplete(){
    var sellerId = $('input[name="seller"]:checked').attr('id')
    var buyerId = $('input[name="buyer"]:checked').attr('id')
    if(sellerId == undefined || buyerId == undefined){
         showErrorAlert("Please select seller and buyer");
        // window.location = 'proposal.html';
        return;
    }
    updateBuyerInfo(buyerId);
    updateSellerInfo(sellerId);
    window.location = 'proposal.html';
}