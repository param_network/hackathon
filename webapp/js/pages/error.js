$(function(){
    checkWeb3Config();    
    errorType();
});
function errorType(){
    var  rid = parseInt(getUrlParameter("id"));
    switch(rid){
        case 1:
            $("#metamaskError").show();
            break;
        case 2:
            $("#networkError").show();
            break;
        case 3:
            $("#unavailableError").show();
            break;
        default:
            break;
    }
}

function checkWeb3Config(){
    debugger;
    if (typeof window.web3 == 'undefined') {
        $("#metamaskError").show();
        $("#networkError").css("display","none");
        $("#unavailableError").css("display","none");
        return;
    }
    web3.currentProvider.publicConfigStore._events.update = function(json){
        var liveStatus = "Live";
        if(json.networkVersion =='loading'){
            liveStatus = json.networkVersion;
            $("#metamaskError").css("display","none");
            $("#networkError").css("display","none");
            $("#unavailableError").css("display","");
        } else if(parseInt(json.networkVersion) == NETWORK_ID){
            history.back();
            return;
        } else{
            $("#metamaskError").css("display","none");
            $("#networkError").css("display","");
            $("#unavailableError").css("display","none");
        }
        $("#connectionStatus").text(liveStatus);
    };
}
         