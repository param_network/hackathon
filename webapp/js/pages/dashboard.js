var widgetIds = ["#totalinitProposals","#totalPurchaseOrders","#totalInvoice","#totalPayments","#totalCreateReceipt"];
function onPostWeb3Init(){
    drawGraph();
    loadDataFromChain();
}
function reset(){
    for(var index =0;index<widgetIds.length;index++){
        $(widgetIds[index]).text("#");
    }
    $("#chart").empty();
}
function loadDataFromChain(){
    updateHeaders();
}
function updateHeaders(stepId){
    var steps = [0,0,0,0,0];
    sopContract.pull.call("receipt","rid", function(e, data){
        if(e){
            return;
        }
        var receiptIds =  toPullJSON(data);
        for(var index=0;index<receiptIds.length;index++){
            var receiptId = receiptIds[index];
            sopContract.pull.call("receipt-"+receiptId,"status", function(e, _step){
                if(e){
                    return;
                }
                var step = toPullJSON(_step);
                step = step[step.length-1];
                steps[step]+=1;
                updateStepUI(steps);
            });
        }
    });
}

function updateStepUI(steps){
    for(index=0;index<widgetIds.length;index++){
        $(widgetIds[index]).text(steps[index]);
    }

}
function getTransactionInfo(step,stepName){
    var current_id = widgetIds[step];
    var count = parseInt($(current_id).text());
    if(count == 0){
        msg = "No " +stepName+ " found";
        showErrorAlert(msg);
        return;
    }
    window.location = "./transinfo.html?step="+step+"";
}

function onStatusUpdate(result){
        reset();
        onPostWeb3Init();
}