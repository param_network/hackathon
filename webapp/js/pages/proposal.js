var productsCart = [];
var prevTime = 0;
function onPostWeb3Init () {
    counter = 1;
    var user = getBuyerId();
    $("#buyerid").val(user);
    // initNewProduct();
    $('#discountPer').keyup(function(){
        updateTableValues();
    });
    $('#taxPer').keyup(function(){
        updateTableValues();        
    });
}
function addNewRow(){
    debugger;
    var date = new Date();
    var id = date.getTime();
    $("table.order-list").append(
        '<tr class="productRoot">'+
            '<td>'+counter+
            '</td>'+ getProductSelect(id)+
            '<td>'+
                '<button class="ibtnDel btn btn-md" id="'+id+'">'+
                    '<i class="fa fa-times" aria-hidden="true"></i>'+
                '</button>'+
                
            '</td>'+

        '</tr>');
        counter++;
      $("table.order-list").on("click", ".ibtnDel", function (event) {
        var ele = $(this);
        for(var index=0;index<productsCart.length;index++){
            if(parseInt(ele.attr('id')) == productsCart[index]){
                productsCart.splice(index, 1);
                break;
            }
        }
        ele.closest("tr").remove();
        updateTableValues();
    });
}

// function initNewProduct(){
//     var user = getBuyerId();
//     $("#buyerid").val(user);
//     $("#addrow").on("click", function () {
//     var date = new Date();
//     var id = date.getTime();
//     $("table.order-list").append(
//         '<tr class="productRoot">'+
//             '<td>'+counter+
//             '</td>'+ getProductSelect(id)+
//             '<td>'+
//                 '<button class="ibtnDel btn btn-md" id="'+id+'">'+
//                     '<i class="fa fa-times" aria-hidden="true"></i>'+
//                 '</button>'+
                
//             '</td>'+

//         '</tr>');
//         counter++;
//     });
    
//     $("table.order-list").on("click", ".ibtnDel", function (event) {
//         var ele = $(this);
//         for(var index=0;index<productsCart.length;index++){
//             if(parseInt(ele.attr('id')) == productsCart[index]){
//                 productsCart.splice(index, 1);
//                 break;
//             }
//         }
//         ele.closest("tr").remove();
//         updateTableValues();
//     });
// }
function getProductSelect(id){
    var htmlData = '<td>'+'<select class="additemSelect proposalInput" id="product'+id+'" onchange="updateTable('+id+')">';
    htmlData+="<option value=-1>Select product</option>";
    for(var index=0;index<PRODUCTS_INFO.length;index++){
        htmlData+="<option value='"+index+"'>"+PRODUCTS_INFO[index].productTitle+"</option>";
    }
    htmlData+="</select></td>";
    htmlData+='</select>'+'</td>'+
              '<td id="brand'+id+'">'+
              '</td>';
    htmlData+='<td>'+
              '<input  type="text" class="form-control proposalInput" name="quantity" id="quantity'+id+'" value="1" onkeyup="updateTable('+id+')"/>'+
              '</td>';          
    htmlData+='<td id="price'+id+'">'+
              '</td>';
    htmlData+='<td id="totalPrice'+id+'">'+
              '</td>';    
    productsCart.push(id);
    return htmlData;
}

function updateTable(id){
    var index = document.getElementById("product"+id).value;
    updateTableValues();
    if(index != -1){
        $("#brand"+id).text(PRODUCTS_INFO[index].brandName+" & "+PRODUCTS_INFO[index].categoryName);
        $("#price"+id).text(PRODUCTS_INFO[index].mPrice);
        var qun = $('#quantity'+id).val();
        if(isNaN(qun)){
            qun=0;
        }
        $("#totalPrice"+id).text(PRODUCTS_INFO[index].mPrice*qun);
        return;
    }
    $("#brand"+id).text("");
    $("#price"+id).text("");
    $("#totalPrice"+id).text("");
}

function updateTableValues(){
    var total = getSubTotal();
    var discountPer = parseFloat($('#discountPer').val());
    var taxPer = parseFloat($('#taxPer').val());
    if(isNaN(discountPer)){
        discountPer = 0;
    }
    if(isNaN(taxPer)){
        taxPer = 0;
    }
    var tax = total*(taxPer/100);
    var discount = total*(discountPer/100);
    
    $('#discountAmount').text(discount);
    $('#taxAmount').text(tax);
    $("#subtotal").text(total);
    var netTotal = parseInt((total+tax-discount)*1000)/1000;
    $("#nettotal").text(netTotal);
}

function toProductsString(){
    var productsJSON = "";
    for(var index=0;index<productsCart.length;index++){
        var productHTMLId = productsCart[index];
        var pIndex = document.getElementById("product"+productHTMLId).value;
        var fill = $('#quantity'+productHTMLId).val();
        var ask = fill;
        var product = PRODUCTS_INFO[pIndex];
        var _taxPercent = parseFloat($("#taxPer").val().trim());
        if(isNaN(_taxPercent)){
            _taxPercent = 0;
        }
        var subTotal = parseInt(product.mPrice*1000);
        var _taxAmount = parseInt(subTotal * (_taxPercent/100));
        productsJSON+= product.productTitle+","+product.categoryName+","+product.brandName+",";
        productsJSON+=ask+","+fill+","+subTotal+","+parseInt(product.gPrice*1000)+","+_taxAmount+"|";
    }
    if(productsJSON.endsWith("|")){
        productsJSON = productsJSON.substring(0,productsJSON.length-1);
    }
    return productsJSON;
}

function getSubTotal(){
    var subTotal = 0;
    for(var index=0;index<productsCart.length;index++){
        var productIndex = productsCart[index];
        var fill = $('#quantity'+productIndex).val();
        var ask = fill;
        var pIndex = document.getElementById("product"+productIndex).value;
        if(pIndex==-1){
            continue;
        }
        var product = PRODUCTS_INFO[pIndex];
        subTotal+=(parseInt(fill)*product.mPrice)
    }
    return subTotal;
}

function saveProposal(){
    debugger;
    showLoading();
    var buyerId = $("#buyerid").val();
    if(!web3.isAddress(buyerId)){
        hideLoading();
        showErrorAlert("Unable to get user address.");
        return;
    }
    
    var productsJSON = toProductsString();
    var subTotal = getSubTotal()*1000;
    var _discountPercent = parseFloat($("#discountPer").val().trim());
    var _taxPercent = parseFloat($("#taxPer").val().trim());
    if(isNaN(_taxPercent)){
        _taxPercent = 0;
    }
    if(isNaN(_discountPercent)){
        _discountPercent = 0;
    }
    var _discountAmount = parseInt(subTotal * (_discountPercent/100));
    var _taxAmount = parseInt(subTotal * (_taxPercent/100));
    var netTotal = subTotal-_discountAmount+_taxAmount;
    unlockSellerAccount();
    var transObj = {from:getSellerId()};
    paramReceiptsContract.initiateProposal.estimateGas(buyerId, productsJSON, parseInt(_discountPercent*10), parseInt(_taxPercent*10), _discountAmount, _taxAmount, subTotal, netTotal, function(err, estGas){
        if(err){
            hideLoading();
            showErrorAlert("Unable to process your order\n"+err.toString());
            return;
        }
        transObj.gas = parseInt(estGas*1.30);
        productsCart = [];
        paramReceiptsContract.initiateProposal.sendTransaction(buyerId, productsJSON, parseInt(_discountPercent*10), parseInt(_taxPercent*10), _discountAmount, _taxAmount, subTotal, netTotal, transObj, function(err, data){
            if(err){
                hideLoading();
                showErrorAlert("Unable to process your order\n"+err.toString());
                return;
            }
            txHash = data;
        });    
    });
}

function cancelProposal(){
    if(productsCart.length==0){
        productsCart = [];
    }
    counter=1;            
    $("#productTableBody").empty();
    $("#discountPer").val('');
    $("#taxPer ").val('');
}

function showLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "block";
      $("body").find("*").attr("disabled", "disabled");
      $("body").find("a").click(function (e) { e.preventDefault(); });
      document.body.style.backgroundColor="#f4f4f4";
      document.body.style.opacity=0.5;
}
function hideLoading(){
      var x = document.getElementById("loadingDiv");
      x.style.display = "none";
      $("body").find("*").removeAttr("disabled");
      $("body").find("a").unbind("click");  
      document.body.style.backgroundColor="transparent";
      document.body.style.opacity=1;  
}

function onStatusUpdate(result){
    if(typeof txHash!=="undefined" && txHash !==null && txHash==result.transactionHash){
        txHash = undefined;
        counter=1; 
        productsCart = [];      
        $("#buyerid").empty();     
        $("#productTableBody").empty();
        $("#discountPer").val('');
        $("#taxPer ").val('');
        window.location = "./timeline.html?id="+result.args.receiptId.toString();
    }
}