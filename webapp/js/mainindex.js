$(function () {
  
});
var optionJSON=[];
function checkModalStatus(){
    var opt = getOption();
    if(opt == -1){
        $('#MetamaskRpcModal').modal('show');
    }
}
function onClickMetamask(){
    $("#rpcContent").hide();
    // optionJSON = {"option":0,"value":""};
    setOption(0,"");
    $('#MetamaskRpcModal').modal('hide');
}
function createCustomRpc(){
    $("#rpcContent").show();
}
function saveCustomRPC(){
    debugger;
    var httpUrl = $("#rpcUrl").val();
    if(httpUrl.length!=0){
        setOption(1,httpUrl);
        $("#rpcUrl").empty();
        $("#rpcContent").hide();
        $("#connectionStatus").text("Live");
        $('#MetamaskRpcModal').modal('hide');
        return;
    }
    showErrorAlert("Please provide the URL");
}
function checkRPC(index){
   debugger;
   var url = getValue();
   if(url == undefined){
       showErrorAlert("Choose one option from the modal");
       $('#MetamaskRpcModal').modal('show');
       return;
   }
   switch(index){
        case 1:
            if(getBuyerId() && getSellerId()){
                window.location = './pages/proposal.html';
                break;
            }
            window.location ="./pages/userprofile.html";
            break;
        case 2:
            window.location ="./pages/dashboard.html";
            break;
        case 3:
            window.location ="./pages/appusecase.html";
            break;
        default:
            break;
   }
}