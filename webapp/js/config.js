const SOP_CONTRACT_ADDRESS = "0xdbea2e5e60e81b5b893270ee7b6679a8969598bc";
const PARAM_CONTRACT_ADDRESS = "0xa69325d4153febde10d9225016ae764698d0ee98";
// const HTTP_URL = "http://18.188.85.144:8446";
// const HTTP_URL;
//  HTTP_URL = getHTTPRPCURL();
 const NETWORK_ID = "72616";

const NETWORK_USERS_INFO = [
                {user:"0x11057dFE5B91fcC785bc53d89C8aD3d2c4A17cd6",password:"abc123"},
                {user:"0x32f48336f4dc0b908dd34a2f50e85cc160da2719",password:"abc123"},
                {user:"0x7fddb397deed0a9253aecdb597f8123cdd4b68ac",password:"abc123"},
                {user:"0x172d6a9156b8426f306a4e261b154ff1f9187530",password:"abc123"},
                {user:"0x0d09d8f53e1c5c2599ef366251b0a92eec5e5d1b",password:"abc123"},
                {user:"0x94f78ed518caf6f26ca4bcb165efa875d8b99e01",password:"abc123"}
            ];

const PRODUCTS_INFO = [{productTitle:'Echo Dot',categoryName:'Electronics/Voice',brandName:'Amazon',mPrice:4499, gPrice:4499},
                    {productTitle:'Sony 101.4 cm TV',categoryName:'Electronics/TV',brandName:'Sony',mPrice: 44999, gPrice: 55900},
                    {productTitle:'Apple MacBook Air MQD32HN/A 13.3-inch Laptop 2017',categoryName:'Electronics/Laptop',brandName:'Apple',mPrice:57990, gPrice:77200},
                    {productTitle:'HP 14q-BU005TU 2017',categoryName:'Electronics/Laptop',brandName:'HP',mPrice:28990, gPrice:33501},
                    {productTitle:'iBall CompBook Exemplaire',categoryName:'Electronics/Laptop',brandName:'iBall',mPrice:13999, gPrice:16499},
                    {productTitle:'The Last Of Us: Remastered (PS4)',categoryName:'Computers & Accessories/Gamezone',brandName:'Sony',mPrice:1346, gPrice:1499},
                    {productTitle:'Call of Duty: Infinite Warfare (PS4)',categoryName:'Computers & Accessories/Gamezone',brandName:'Sony',mPrice:599, gPrice:1299},
                    {productTitle:'HyperX Cloud Stinger Gaming Headset',categoryName:'Computers & Accessories/Gamezone',brandName:'HyperX',mPrice:3805, gPrice:4499},
                    {productTitle:'Canon EOS 1300D 18MP',categoryName:'Electronics/Cameras & Photography',brandName:'Canon',mPrice:24564, gPrice:29995},
                    {productTitle:'Canon EOS 1500D 24.1MP',categoryName:'Electronics/Cameras & Photography',brandName:'Canon',mPrice:37990, gPrice:46995},
                    {productTitle:'WD My Passport 1TB',categoryName:'Computers & Accessories/Data Storage',brandName:'WD',mPrice:3799, gPrice:5960},
                    {productTitle:'Zebronics BT4440RUCF 4.1',categoryName:'Electronics/Home Audio',brandName:'Zebronics',mPrice:2349, gPrice:2929},
                    {productTitle:'Apple iPad(6th Gen) Tablet (9.7 inch&comma;32GB&comma; Wi-Fi)',categoryName:'Computers & Accessories/Tablets',brandName:'Apple',mPrice:27709, gPrice:28000},
                ];

function getBuyerId(){
    return localStorage.buyerId;
}

function getSellerId(){
    return localStorage.sellerId;
}

function setOption(option, value){
    localStorage.option = JSON.stringify({"option":option,"value":value});
}

function getOption(){
    if(localStorage.option){
        var json = JSON.parse(localStorage.option);
        return json.option;
    }
    return -1;
}
function getValue(){
    if(localStorage.option){
        var json = JSON.parse(localStorage.option);
        return json.value;
    }
    return "";
}

function getSum(sum){
    sum= parseInt(sum);
    return sum/1000;
}

function getPercentage(per){
    return per/10;
}
function unlockBuyerAccount(){
    if(getOption() == 1)
        web3.personal.unlockAccount(localStorage.buyerId,"abc123");
}
function unlockSellerAccount(){
    if(getOption() == 1)
        web3.personal.unlockAccount(localStorage.sellerId,"abc123");
}
var STATES = ['PROPOSAL','PO','INVOICE','PAYMENT','RECEIPT','CANCEL'];
function getStep(state){
    return STATES[parseInt(state)];
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
function showErrorAlert(message){
     swal("Error!",message, "error");
}